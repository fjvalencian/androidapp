package sterio.me.customview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by jacevedo on 21-01-15.
 */
public class CircleView extends View
{
    private Paint paintFirstCircle;
    private Paint paintSecondCircle;
    private Paint paintText;
    private int colorFirstCircle;
    private int textSecondCircle;



    public CircleView(Context context)
    {
        super(context);
        init();
    }

    public CircleView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
    }

    public CircleView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init()
    {
        colorFirstCircle = Color.GREEN;
        paintFirstCircle = new Paint();


        paintSecondCircle = new Paint();
        paintSecondCircle.setColor(Color.rgb(208,2,27));

        paintText = new Paint();
        paintText.setColor(Color.WHITE);
        paintText.setTextSize(15);

        textSecondCircle = 1;
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);
        paintFirstCircle.setColor(colorFirstCircle);
        drawFirstCircle(canvas);
        if(textSecondCircle > 0)
        {
            drawSecondCircle(canvas);
            putNumber(canvas);
        }
    }

    private void putNumber(Canvas canvas)
    {
        float hundredPercentage = 0;
        if(getHeight()<=getWidth())
        {
            hundredPercentage = getHeight();
        }
        else
        {
            hundredPercentage = getWidth();
        }

        float radioSecondCircle  = (hundredPercentage * 30) /100;
        float radio =  ((hundredPercentage * 72) /100);
        canvas.drawText(textSecondCircle+"",radio-5,radioSecondCircle/2+10,paintText);
    }

    private void drawSecondCircle(Canvas canvas)
    {
        float hundredPercentage = 0;
        if(getHeight()<=getWidth())
        {
            hundredPercentage = getHeight();
        }
        else
        {
            hundredPercentage = getWidth();
        }

        float radioSecondCircle  = (hundredPercentage * 30) /100;
        canvas.drawCircle(hundredPercentage-radioSecondCircle,(radioSecondCircle/2)+5,radioSecondCircle/2,paintSecondCircle);
    }

    private void drawFirstCircle(Canvas canvas)
    {
        float hundredPercentage = 0;
        if(getHeight()<=getWidth())
        {
            hundredPercentage = getHeight();
        }
        else
        {
            hundredPercentage = getWidth();
        }
        float radio =  ((hundredPercentage * 72) /100)/2;
        canvas.drawCircle(radio+5,radio+5,radio,paintFirstCircle);
    }

    public int getTextSecondCircle()
    {
        return textSecondCircle;
    }

    public void setTextSecondCircle(int textSecondCircle)
    {
        this.textSecondCircle = textSecondCircle;
        invalidate();
        requestFocus();
    }

    public int getColorFirstCircle()
    {
        return colorFirstCircle;
    }

    public void setColorFirstCircle(int colorFirstCircle)
    {
        this.colorFirstCircle = colorFirstCircle;
        invalidate();
        requestFocus();
    }
    public void removeNotifications()
    {
        textSecondCircle = 0;
        invalidate();
        requestFocus();
    }
}
