package sterio.me.customview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by jacevedo on 27-01-15.
 */
public class CircleGraphLevel extends View
{
    private int backgroundColor;
    private int levelColor;
    private int borderColor;
    private int percentage;
    private Paint paintBackground;
    private Paint paintBorder;
    private Paint paintLevel;


    public CircleGraphLevel(Context context)
    {
        super(context);
        init();
    }

    public CircleGraphLevel(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
    }

    public CircleGraphLevel(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init()
    {
        backgroundColor = Color.WHITE;
        levelColor = Color.RED;
        borderColor = Color.BLUE;

        paintBackground = new Paint();
        paintBorder = new Paint();
        paintLevel = new Paint();

        percentage = 10;
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);
        putColors();
        getCroppedBitmap(canvas);
        canvas.drawBitmap(getInsideInfo(),0,0,new Paint());

    }

    private void putColors()
    {
        paintBackground.setColor(backgroundColor);
        paintBorder.setColor(borderColor);
        paintLevel.setColor(levelColor);
    }

    public void getCroppedBitmap(Canvas canvas)
    {
        int measure = getHeight() <= getWidth() ? getHeight() : getWidth();
        canvas.drawCircle(measure / 2, measure/ 2,
                measure / 2, paintBorder);


    }
    public Bitmap getInsideInfo()
    {
        int measure = getHeight() <= getWidth() ? getHeight() : getWidth();
        int measure2 = measure * 97/100;

        Bitmap output = Bitmap.createBitmap( measure,
                measure, Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(output);
        canvas.drawCircle(measure / 2, measure/ 2,
                measure2/ 2, paintBackground);

        final Rect rect = new Rect(0, measure * (100 - percentage)/100, measure, measure);
        canvas.drawARGB(0, 0, 0, 0);

        paintLevel.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));

        canvas.drawRect(rect,paintLevel);

        return output;
    }

    public int getBackgroundColor()
    {
        return backgroundColor;
    }

    @Override
    public void setBackgroundColor(int backgroundColor)
    {
        this.backgroundColor = backgroundColor;
        invalidate();
        requestFocus();
    }

    public int getLevelColor()
    {
        return levelColor;
    }

    public void setLevelColor(int levelColor)
    {
        this.levelColor = levelColor;
        invalidate();
        requestFocus();
    }

    public int getBorderColor()
    {
        return borderColor;
    }

    public void setBorderColor(int borderColor)
    {
        this.borderColor = borderColor;
        invalidate();
        requestFocus();
    }
    public int getPercentage()
    {
        return percentage;
    }

    public void setPercentage(int percentage)
    {
        this.percentage = percentage;
        invalidate();
        requestFocus();
    }


}
