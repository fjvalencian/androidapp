package sterio.me.customview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by jacevedo on 19-01-15.
 */
public class CircleGraph extends View
{
    private Paint firstCircle;
    private Paint firstInnerCircle;
    private Paint secondCircle;
    private int colorSecondCircle;
    private int margin;
    private double percentage;


    public CircleGraph(Context context)
    {
        super(context);
        init();
    }

    public CircleGraph(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
    }

    public CircleGraph(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init()
    {
        colorSecondCircle = Color.argb(255,20,71,88);
        firstCircle = new Paint();
        firstCircle.setColor(Color.rgb(20,71,88));

        firstInnerCircle = new Paint();
        firstInnerCircle.setColor(Color.WHITE);

        secondCircle = new Paint();
        margin = 6;
        percentage = 90;

    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);
        drawFirstCircle(canvas);
        drawSecondCircle(canvas);
    }

    private void drawSecondCircle(Canvas canvas)
    {
        float hundredPercent;
        secondCircle.setColor(colorSecondCircle);

        if(getHeight()<=getWidth())
        {
            hundredPercent = (getHeight()/2)-margin;
        }
        else
        {
            hundredPercent = (getWidth()/2)-margin;
        }

        float radius = (float)(hundredPercent *(percentage/100));


        canvas.drawCircle(getWidth()/2, getHeight()/2, radius, secondCircle);
    }

    private void drawFirstCircle(Canvas canvas)
    {
        if(getHeight()<=getWidth())
        {
            canvas.drawCircle(getWidth()/2, getHeight()/2, getHeight()/2, firstCircle);
            canvas.drawCircle(getWidth()/2, getHeight()/2, (getHeight()/2)-margin, firstInnerCircle);
        }
        else
        {
            canvas.drawCircle(getWidth()/2, getHeight()/2, getWidth()/2, firstCircle);
            canvas.drawCircle(getWidth()/2, getHeight()/2, (getWidth()/2)-margin, firstInnerCircle);
        }

    }

    public int getColorSecondCircle()
    {
        return colorSecondCircle;
    }

    public void setColorSecondCircle(int colorSecondCircle)
    {
        this.colorSecondCircle = colorSecondCircle;
        invalidate();
        requestFocus();
    }

    public void setMargin(int margin)
    {
        this.margin = margin;
        invalidate();
        requestFocus();
    }

    public double getPercentage()
    {
        return percentage;
    }

    public void setPercentage(double percentage)
    {
        this.percentage = percentage;
        invalidate();
        requestFocus();
    }
}
