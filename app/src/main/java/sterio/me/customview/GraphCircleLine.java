package sterio.me.customview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.TextView;

import sterio.me.steriomeapp.R;

/**
 * Created by jacevedo on 27-01-15.
 */
public class GraphCircleLine extends View implements Animation.AnimationListener
{
    private static final int FADE_MILLISECONDS = 1000;
    private static final int FADE_STEP = 120;
    private static final int ALPHA_STEP = 255 / (FADE_MILLISECONDS / FADE_STEP);


    private int colorCircle;
    private int colorLine;
    private Paint paintCircle;
    private Paint paintCircleAlpha;
    private Paint paintLine;
    private Paint paintLineAlpha;
    private double value;
    private double sumValue;
    private TextView txtMessage;
    private TextView txtPercentage;
    private Button btnNext;
    private TextView txtTextTen;
    private int currentAlpha;
    private Button btnInsig;

    public GraphCircleLine(Context context)
    {
        super(context);
        init();
    }

    public GraphCircleLine(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
    }

    public GraphCircleLine(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init()
    {
        colorCircle = getContext().getResources().getColor(R.color.cruise);
        colorLine = Color.WHITE;

        paintCircle = new Paint();
        paintCircle.setColor(colorCircle);

        paintCircleAlpha = new Paint();

        paintLineAlpha  = new Paint();
        paintLineAlpha.setColor(colorLine);


        paintLine = new Paint();
        paintLine.setColor(colorLine);
        value = 96;
        sumValue = 0;
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);
        if (sumValue <= value)
        {
            drawFirstCircle(canvas);
            drawSecondCircle(canvas, sumValue);
            drawThirdCircle(canvas);
            sumValue = sumValue + 0.9;
            if (txtPercentage != null)
            {
                txtPercentage.setText(getTextUnit((int) sumValue));
                txtTextTen.setText(getTextTen((int) sumValue));
            }
            invalidate();

        }
        else
        {

            drawFirstCircle(canvas);
            drawSecondCircle(canvas, value);
            drawThirdCircle(canvas);
            if (txtPercentage != null)
            {
                txtPercentage.setText(getTextUnit((int) value));
                txtTextTen.setText(getTextTen((int) value));
                txtMessage.setText(getMessage((int) value));
                btnNext.setEnabled(true);
                btnInsig.setEnabled(true);
                createAnim();

            }
            if(currentAlpha <= (255-ALPHA_STEP))
            {
                currentAlpha = currentAlpha + ALPHA_STEP;
                drawCircleAlpha(canvas,value);
                postInvalidateDelayed(FADE_STEP);
            }
            else
            {
                currentAlpha = 255;
                drawCircleAlpha(canvas,value);
            }

        }

    }

    private void drawCircleAlpha(Canvas canvas, double value)
    {
        drawFirstCircleAlpha(canvas, value);
        drawSecondCircleAlpha(canvas, value);
        drawThirdCircleAlpha(canvas);
    }

    private void drawSecondCircleAlpha(Canvas canvas, double value)
    {
        paintLineAlpha.setAlpha(currentAlpha);

        float size = getWidth() <= getHeight() ? getWidth() : getHeight();
        size = size * 95 / 100;

        RectF rect = new RectF();
        rect.set(size * 5 / 100, size * 5 / 100, size, size);
        canvas.drawArc(rect, -90, 360 * ((float) value) / 100, true, paintLineAlpha);
    }

    private void drawFirstCircleAlpha(Canvas canvas, double value)
    {
        paintCircleAlpha.setColor(getColor(value));
        paintCircleAlpha.setAlpha(currentAlpha);

        float size = getWidth() <= getHeight() ? getWidth() : getHeight();

        canvas.drawCircle(size / 2, size / 2, size / 2, paintCircleAlpha);
    }

    private void drawThirdCircleAlpha(Canvas canvas)
    {
        float size = getWidth() <= getHeight() ? getWidth() : getHeight();
        float size2 = size * 80 / 100;

        canvas.drawCircle(size / 2, size / 2, size2 / 2, paintCircleAlpha);
    }

    private String getTextUnit(int sumValue)
    {
        if(sumValue<10)
        {
            return sumValue+"";
        }
        else if(sumValue<=99)
        {
            return (sumValue+"").substring(1);
        }
        return (sumValue+"").substring(2);

    }

    private String getTextTen(int sumValue)
    {
        if(sumValue<10)
        {
            return "";
        }
        return (sumValue+"").substring(0,(sumValue+"").length()-1);
    }

    private void createAnim()
    {
        AlphaAnimation anim = new AlphaAnimation(0.0f,1.0f);
        anim.setDuration(FADE_MILLISECONDS);
        anim.setRepeatCount(0);
        anim.setAnimationListener(this);
        txtMessage.startAnimation(anim);
        btnNext.startAnimation(anim);
        btnInsig.startAnimation(anim);
    }

    private String getMessage(int value)
    {
        if(value > 0 && value <= 9)
        {
            return "Damn! Next time will be better.";
        }
        if(value > 9 && value <= 19)
        {
            return "Oh no! You did get some right though.";
        }
        if(value > 19 && value <= 29)
        {
            return "Don't give up!";
        }
        if(value > 29 && value <= 39)
        {
            return "Practice makes perfect!";
        }
        if(value > 39 && value <= 49)
        {
            return "Nearly got there.";
        }
        if(value > 49 && value <= 59)
        {
            return "Phew! Just over the line.";
        }
        if(value > 59 && value <= 69)
        {
            return "Well done. You passed!";
        }
        if(value > 69 && value <= 79)
        {
            return "Good work this time!";
        }
        if(value > 79 && value <= 89)
        {
            return "Great job! ";
        }
        if(value > 89 && value <= 99)
        {
            return "Excellent work!";
        }
        if(value > 100)
        {
            return "You're on fire! Perfect score";
        }
        return "error";
    }

    private void drawFirstCircle(Canvas canvas)
    {
        float size = getWidth() <= getHeight() ? getWidth() : getHeight();

        canvas.drawCircle(size / 2, size / 2, size / 2, paintCircle);

    }

    private void drawSecondCircle(Canvas canvas, double innerValue)
    {
        float size = getWidth() <= getHeight() ? getWidth() : getHeight();
        size = size * 95 / 100;

        RectF rect = new RectF();
        rect.set(size * 5 / 100, size * 5 / 100, size, size);
        canvas.drawArc(rect, -90, 360 * ((float) innerValue) / 100, true, paintLine);

    }

    private int getColor(double innerValue)
    {
        int color = 0;
        if(innerValue>=0 && innerValue<=20)
        {
            color = getContext().getResources().getColor(R.color.monza);
        }
        if(innerValue>=21 && innerValue<=40)
        {
            color = getContext().getResources().getColor(R.color.jaffa);
        }
        if(innerValue>=41 && innerValue<=60)
        {
            color = getContext().getResources().getColor(R.color.confetti);
        }
        if(innerValue>=61 && innerValue<=80)
        {
            color = getContext().getResources().getColor(R.color.manz);
        }
        if(innerValue>=81 && innerValue<=100)
        {
            color = getContext().getResources().getColor(R.color.laser);
        }
        return color;
    }

    private void drawThirdCircle(Canvas canvas)
    {

        float size = getWidth() <= getHeight() ? getWidth() : getHeight();
        float size2 = size * 80 / 100;

        canvas.drawCircle(size / 2, size / 2, size2 / 2, paintCircle);
    }

    public int getColorCircle()
    {
        return colorCircle;
    }

    public void setColorCircle(int colorCircle)
    {
        this.colorCircle = colorCircle;
        invalidate();
        requestFocus();
    }

    public int getColorLine()
    {
        return colorLine;
    }

    public void setColorLine(int colorLine)
    {
        this.colorLine = colorLine;
        invalidate();
        requestFocus();
    }

    public void setValue(double value)
    {
        this.value = value;
        this.sumValue = 0;
    }

    public void setEdtMessage(TextView txtScore)
    {
        this.txtMessage = txtScore;
    }

    public void setEdtTextScore(TextView txtPercentage)
    {
        this.txtPercentage = txtPercentage;
    }

    public void setBtnNext(Button btnNext)
    {
        this.btnNext = btnNext;
    }


    @Override
    public void onAnimationStart(Animation animation)
    {
        txtMessage.setVisibility(View.VISIBLE);
        btnNext.setVisibility(View.VISIBLE);
        btnInsig.setVisibility(View.VISIBLE);

    }

    @Override
    public void onAnimationEnd(Animation animation)
    {

    }

    @Override
    public void onAnimationRepeat(Animation animation)
    {

    }

    public void setEdtTextTen(TextView txtTextTen)
    {
        this.txtTextTen = txtTextTen;
    }

    public void setBtnInsig(Button btnInsig)
    {
        this.btnInsig = btnInsig;
    }
}
