package sterio.me.object.adapter;

/**
 * Created by jacevedo on 22-01-15.
 */
public class InsightsScoreObject
{
    private String moduleName;
    private String score;
    private int type;

    public InsightsScoreObject()
    {

    }

    public InsightsScoreObject(String moduleName, String score, int type)
    {
        this.moduleName = moduleName;
        this.score = score;
        this.type = type;
    }

    public String getModuleName()
    {
        return moduleName;
    }

    public void setModuleName(String moduleName)
    {
        this.moduleName = moduleName;
    }

    public String getScore()
    {
        return score;
    }

    public void setScore(String score)
    {
        this.score = score;
    }

    public int getType()
    {
        return type;
    }

    public void setType(int type)
    {
        this.type = type;
    }
}
