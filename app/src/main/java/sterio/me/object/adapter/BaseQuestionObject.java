package sterio.me.object.adapter;

/**
 * Created by jacevedo on 27-01-15.
 */
public abstract class BaseQuestionObject
{
    private String question;

    protected BaseQuestionObject(String question)
    {
        this.question = question;
    }

    public abstract String whoAmi();


    public String getQuestion()
    {
        return question;
    }

    public void setQuestion(String question)
    {
        this.question = question;
    }
}
