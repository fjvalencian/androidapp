package sterio.me.object.adapter;

/**
 * Created by jacevedo on 27-01-15.
 */
public class TextQuestionObject extends BaseQuestionObject
{
    private String response;
    private boolean goodAnswer;

    public TextQuestionObject(String question,String response, boolean goodAnswer)
    {
        super(question);
        this.response = response;
        this.goodAnswer = goodAnswer;
    }

    @Override
    public String whoAmi()
    {
        return "TextQuestion";
    }

    public String getResponse()
    {
        return response;
    }

    public void setResponse(String response)
    {
        this.response = response;
    }

    public boolean isGoodAnswer()
    {
        return goodAnswer;
    }

    public void setGoodAnswer(boolean goodAnswer)
    {
        this.goodAnswer = goodAnswer;
    }
}
