package sterio.me.object.adapter;

/**
 * Created by jacevedo on 27-01-15.
 */
public class VoiceQuestionObject extends BaseQuestionObject
{
    private String urlSound;
    private boolean goodAnswer;

    public VoiceQuestionObject(String question, String urlSound, boolean goodAnswer)
    {
        super(question);
        this.urlSound = urlSound;
        this.goodAnswer = goodAnswer;
    }

    @Override
    public String whoAmi()
    {
        return "VoiceOption";
    }

    public String getUrlSound()
    {
        return urlSound;
    }

    public void setUrlSound(String urlSound)
    {
        this.urlSound = urlSound;
    }

    public boolean isGoodAnswer()
    {
        return goodAnswer;
    }

    public void setGoodAnswer(boolean goodAnswer)
    {
        this.goodAnswer = goodAnswer;
    }
}
