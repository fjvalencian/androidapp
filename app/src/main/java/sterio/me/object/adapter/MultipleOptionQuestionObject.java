package sterio.me.object.adapter;

/**
 * Created by jacevedo on 27-01-15.
 */
public class MultipleOptionQuestionObject extends BaseQuestionObject
{
    private int correctQuestion;
    private int selectedQuestion;

    public MultipleOptionQuestionObject(String question, int correctQuestion, int selectedQuestion)
    {
        super(question);
        this.correctQuestion = correctQuestion;
        this.selectedQuestion = selectedQuestion;
    }

    @Override
    public String whoAmi()
    {
        return "MultipleOption";
    }

    public int getCorrectQuestion()
    {
        return correctQuestion;
    }

    public void setCorrectQuestion(int correctQuestion)
    {
        this.correctQuestion = correctQuestion;
    }

    public int getSelectedQuestion()
    {
        return selectedQuestion;
    }

    public void setSelectedQuestion(int selectedQuestion)
    {
        this.selectedQuestion = selectedQuestion;
    }
}
