package sterio.me.object.response;

/**
 * Created by jacevedo on 27-01-15.
 */
public class ResponseLogin
{
    private boolean state;
    private String comments;
    private int numberLearning;
    private int numberNotification;


    public boolean isState()
    {
        return state;
    }

    public void setState(boolean state)
    {
        this.state = state;
    }

    public String getComments()
    {
        return comments;
    }

    public void setComments(String comments)
    {
        this.comments = comments;
    }

    public int getNumberLearning()
    {
        return numberLearning;
    }

    public void setNumberLearning(int numberLearning)
    {
        this.numberLearning = numberLearning;
    }

    public int getNumberNotification()
    {
        return numberNotification;
    }

    public void setNumberNotification(int numberNotification)
    {
        this.numberNotification = numberNotification;
    }
}
