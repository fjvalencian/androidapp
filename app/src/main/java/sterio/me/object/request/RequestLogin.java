package sterio.me.object.request;

import java.util.Date;

/**
 * Created by jacevedo on 27-01-15.
 */
public class RequestLogin
{
    private String user;
    private String pass;
    private String token;

    public RequestLogin()
    {

    }
    public RequestLogin(String user, String pass, String token)
    {
        this.user = user;
        this.pass = pass;
        this.token = token;
    }

    public String getUser()
    {
        return user;
    }

    public void setUser(String user)
    {
        this.user = user;
    }

    public String getPass()
    {
        return pass;
    }

    public void setPass(String pass)
    {
        this.pass = pass;
    }

    public String getToken()
    {
        return token;
    }

    public void setToken(String token)
    {
        this.token = token;
    }
}
