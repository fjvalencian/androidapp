package sterio.me.steriomeapp;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

import sterio.me.util.PutFonts;
import sterio.me.util.Validations;


public class SingUp extends Activity implements View.OnClickListener
{
    private TextView txtLineOne;
    private TextView txtLineTwo;
    private TextView txtLineBack;
    private TextView txtBadLearnerCode;
    private TextView txtBadFullName;
    private TextView txtBadMail;
    private TextView txtBadMobile;
    private TextView txtBadPassword;
    private TextView txtBadRepeatPassword;
    private EditText edtLearnerCode;
    private EditText edtFullName;
    private EditText edtMail;
    private EditText edtMobile;
    private EditText edtPassword;
    private EditText edtRepeatPassword;
    private Button btnLogin;
    private boolean isCorrectLearnerCode;
    private boolean isCorrectFullName;
    private boolean isCorrectMail;
    private boolean isCorrectMobile;
    private boolean isCorrectPassword;
    private boolean isCorrectPassword2;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sing_up);
        initComponents();
    }

    private void initComponents()
    {
        isCorrectLearnerCode = true;
        isCorrectFullName = true;
        isCorrectMail = true;
        isCorrectMobile = true;
        isCorrectPassword = true;

        txtLineOne = (TextView)findViewById(R.id.txtLineOneSignUp);
        txtLineTwo = (TextView)findViewById(R.id.txtLineTwoSignUp);
        txtLineBack = (TextView)findViewById(R.id.txtBack);
        txtBadLearnerCode = (TextView)findViewById(R.id.txtBadLearnerCode);
        txtBadFullName = (TextView)findViewById(R.id.txtBadFullName);
        txtBadMail = (TextView)findViewById(R.id.txtBadMail);
        txtBadMobile = (TextView)findViewById(R.id.txtBadMobile);
        txtBadPassword = (TextView)findViewById(R.id.txtBadPassword);
        txtBadRepeatPassword = (TextView)findViewById(R.id.txtBadRepeatPassword);
        edtLearnerCode = (EditText)findViewById(R.id.edtLineOneSignUp);
        edtFullName = (EditText)findViewById(R.id.edtLineTwoSignUp);
        edtMail = (EditText)findViewById(R.id.edtLineThreeSignUp);
        edtMobile = (EditText)findViewById(R.id.edtLineFourSignUp);
        edtPassword = (EditText)findViewById(R.id.edtLineFiveSignUp);
        edtRepeatPassword = (EditText)findViewById(R.id.edtLineSixSignUp);
        btnLogin = (Button)findViewById(R.id.btnSignUp);

        putFont();

        btnLogin.setOnClickListener(this);
        txtLineBack.setOnClickListener(this);
    }

    private void putFont()
    {
        PutFonts fonts = new PutFonts(this);
        fonts.putFonts(txtLineOne, txtLineTwo, txtLineBack, txtBadLearnerCode, txtBadFullName,
                txtBadMail, txtBadMobile, txtBadPassword, txtBadRepeatPassword);
        fonts.putFonts(edtLearnerCode, edtFullName, edtMail, edtMobile, edtPassword,
                edtRepeatPassword);
        fonts.putFonts(btnLogin);
    }


    @Override
    public void onClick(View v)
    {
        Intent i;
        switch (v.getId())
        {
            case R.id.btnSignUp:
                Validations validation = new Validations(this);
                isCorrectLearnerCode = validateLearnerCode(validation);
                isCorrectFullName = validateFullName(validation);
                isCorrectMail = validateMail(validation);
                isCorrectMobile = validateMobile(validation);
                isCorrectPassword = validatePassword(validation);
                if(isCorrectLearnerCode && isCorrectFullName && isCorrectMail && isCorrectMobile &&
                        isCorrectPassword)
                {
                    i = new Intent(this, CreateProfile.class);
                    startActivity(i);
                    finish();
                }
                break;
            case R.id.txtBack:
                i = new Intent(this,LoginActivity.class);
                startActivity(i);
                finish();
                break;
        }
    }

    private boolean validatePassword(Validations validation)
    {
        if(validation.validatePassword(edtPassword.getText().toString(),
                    edtRepeatPassword.getText().toString()))
        {
            validation.goodAnswer(txtBadRepeatPassword,edtPassword,edtRepeatPassword);
            return true;
        }
        validation.badAnswer(txtBadRepeatPassword,edtPassword,edtRepeatPassword);
        return false;
    }

    private boolean validateMobile(Validations validation)
    {
        if(validation.validatePhone(edtMobile.getText().toString()))
        {
            validation.goodAnswer(txtBadMobile,edtMobile);
            return true;
        }
        validation.badAnswer(txtBadMobile, edtMobile);
        return false;
    }

    private boolean validateMail(Validations validation)
    {
        if(validation.validateMail(edtMail.getText().toString()))
        {
            validation.goodAnswer(txtBadMail,edtMail);
            return true;
        }
        validation.badAnswer(txtBadMail,edtMail);;
        return false;
    }

    private boolean validateFullName(Validations validation)
    {
        if(validation.validateNames(edtFullName.getText().toString()))
        {
            validation.goodAnswer(txtBadFullName,edtFullName);
            return true;
        }
        validation.badAnswer(txtBadFullName,edtFullName);
        return false;
    }

    private boolean validateLearnerCode(Validations validation)
    {
        if(validation.validateLearnerCode(edtLearnerCode.getText().toString()))
        {
            validation.goodAnswer(txtBadLearnerCode,edtLearnerCode);
            return true;
        }
       validation.badAnswer(txtBadLearnerCode,edtLearnerCode);
        return false;
    }
    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putBoolean("isCorrectLearnerCode", isCorrectLearnerCode);
        outState.putBoolean("isCorrectFullName", isCorrectFullName);
        outState.putBoolean("isCorrectMail", isCorrectMail);
        outState.putBoolean("isCorrectMobile", isCorrectMobile);
        outState.putBoolean("isCorrectPassword", isCorrectPassword);
        outState.putBoolean("isCorrectPassword2",isCorrectPassword2);
        outState.putString("edtLearnerCode", edtLearnerCode.getText().toString());
        outState.putString("edtFullName", edtFullName.getText().toString());
        outState.putString("edtMail", edtMail.getText().toString());
        outState.putString("edtPhone", edtMobile.getText().toString());
        outState.putString("edtPassword", edtPassword.getText().toString());
        outState.putString("edtPassword2", edtRepeatPassword.getText().toString());

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);
        isCorrectLearnerCode = savedInstanceState.getBoolean("isCorrectLearnerCode");
        isCorrectFullName = savedInstanceState.getBoolean("isCorrectFullName");
        isCorrectMail = savedInstanceState.getBoolean("isCorrectMail");
        isCorrectMobile = savedInstanceState.getBoolean("isCorrectMobile");
        isCorrectPassword = savedInstanceState.getBoolean("isCorrectPassword");
        isCorrectPassword2 =  savedInstanceState.getBoolean("isCorrectPassword2");

        validation(isCorrectLearnerCode,isCorrectFullName,isCorrectMail,isCorrectMobile,isCorrectPassword, isCorrectPassword2);



        String textLearnerCode = savedInstanceState.getString("edtLearnerCode");
        String textFullName = savedInstanceState.getString("edtFullName");
        String textMail = savedInstanceState.getString("edtMail");
        String textPhone = savedInstanceState.getString("edtPhone");
        String textPassword = savedInstanceState.getString("edtPassword");
        String textPassword2 = savedInstanceState.getString("edtPassword2");


        edtLearnerCode.setText(textLearnerCode);
        edtFullName.setText(textFullName);
        edtMail.setText(textMail);
        edtMobile.setText(textPhone);
        edtPassword.setText(textPassword);
        edtRepeatPassword.setText(textPassword2);
    }

    private void validation(boolean isCorrectLearnerCode, boolean isCorrectFullName,
                            boolean isCorrectMail, boolean isCorrectMobile,
                            boolean isCorrectPassword, boolean isCorrectPassword2)
    {
        Validations validations = new Validations(this);
        if(!isCorrectLearnerCode)
        {
            validateLearnerCode(validations);
        }
        if(!isCorrectFullName)
        {
            validateFullName(validations);
        }
        if(!isCorrectMail)
        {
            validateMail(validations);
        }
        if(!isCorrectMobile)
        {
            validateMobile(validations);
        }
        if(!isCorrectPassword || !isCorrectPassword2)
        {
            validatePassword(validations);
        }
    }
}
