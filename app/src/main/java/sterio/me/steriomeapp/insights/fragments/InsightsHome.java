package sterio.me.steriomeapp.insights.fragments;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import sterio.me.adapters.InsightsAdapter;
import sterio.me.customview.CircleGraph;
import sterio.me.customview.CircleGraphLevel;
import sterio.me.steriomeapp.insights.activities.InsightsActivity;
import sterio.me.object.adapter.InsightsScoreObject;
import sterio.me.steriomeapp.R;

public class InsightsHome extends Fragment implements View.OnClickListener
{


    private TextView txtTitleInsightsHome;
    private TextView txtModulesViews;
    private TextView txtTop;
    private TextView txtModulePass;
    private TextView txtHoursLearning;
    private TextView txtImprovement;

    private TextView txtPorcentajeModulesViews;
    private TextView txtTextTop;
    private TextView txtModules;
    private TextView txtHoursLearningText;
    private TextView txtImprovementText;

    private CircleGraphLevel chartOne;
    private CircleGraphLevel chartTwo;
    private ListView lstScore;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragment_insigths_home, container, false);
        initComponents(v);
        return v;
    }

    
    private void initComponents(View v)
    {


        chartOne = (CircleGraphLevel)v.findViewById(R.id.chartOne);
        chartTwo = (CircleGraphLevel)v.findViewById(R.id.chartTwo);


        txtTitleInsightsHome = (TextView)v.findViewById(R.id.txtTitleInsightsHome);
        txtModulesViews = (TextView)v.findViewById(R.id.txtModulesViews);
        txtTop = (TextView)v.findViewById(R.id.txtTop);
        txtModulePass = (TextView)v.findViewById(R.id.txtModulePass);
        txtHoursLearning = (TextView)v.findViewById(R.id.txtHoursLearning);
        txtImprovement = (TextView)v.findViewById(R.id.txtImprovement);
        txtPorcentajeModulesViews = (TextView)v.findViewById(R.id.txtPorcentajeModulesViews);
        txtTextTop = (TextView)v.findViewById(R.id.txtTextTop);
        txtModules = (TextView)v.findViewById(R.id.txtModules);
        txtHoursLearningText = (TextView)v.findViewById(R.id.txtHoursLearningText);
        txtImprovementText = (TextView)v.findViewById(R.id.txtImprovementText);

        lstScore = (ListView)v.findViewById(R.id.lstScore);

        chartOne.setPercentage(85);
        chartOne.setBorderColor(Color.rgb(20, 71, 88));
        chartOne.setBackgroundColor(Color.WHITE);
        chartOne.setLevelColor(Color.rgb(232,205,92));




        chartTwo.setPercentage(88);
        chartTwo.setBorderColor(Color.rgb(20, 71, 88));
        chartTwo.setBackgroundColor(Color.WHITE);
        chartTwo.setLevelColor(Color.rgb(174,207,116));

        lstScore.setAdapter(new InsightsAdapter(getListObject(),getActivity()));

        chartOne.setOnClickListener(this);


        putTypography(txtTitleInsightsHome, txtModulesViews, txtTop, txtModulePass, txtHoursLearning,
                txtImprovement, txtPorcentajeModulesViews, txtTextTop, txtModules,
                txtHoursLearningText,txtImprovementText);
    }

    private ArrayList<InsightsScoreObject> getListObject()
    {
        ArrayList<InsightsScoreObject> listObjects = new ArrayList<>();
        listObjects.add(new InsightsScoreObject("Modules completed:", "Your score:",0));
        listObjects.add(new InsightsScoreObject("Math: Calculus","80",1));
        listObjects.add(new InsightsScoreObject("English: Verb","65",1));
        listObjects.add(new InsightsScoreObject("Geography: Oceans","83",1));
        listObjects.add(new InsightsScoreObject("Biology: Reproduction III","21",1));
        listObjects.add(new InsightsScoreObject("Physics: Velocity II","44",1));

        return listObjects;
    }

    private void putTypography(TextView... texts)
    {
        Typeface myTypeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand_Bold.otf");

        for(TextView text : texts)
        {
            text.setTypeface(myTypeface);
        }

    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.chartOne:
                ((InsightsActivity)getActivity()).selectItemBackStack(1);
                break;
        }
    }
}
