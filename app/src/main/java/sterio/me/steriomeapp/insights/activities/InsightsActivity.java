package sterio.me.steriomeapp.insights.activities;

import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import sterio.me.base.ActivityBase;
import sterio.me.customview.CircleView;
import sterio.me.steriomeapp.insights.fragments.InsightsHome;
import sterio.me.steriomeapp.insights.fragments.ViewInsights;
import sterio.me.steriomeapp.R;

public class InsightsActivity extends ActivityBase
{


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(savedInstanceState==null)
        {
            selectItem(0);
        }
        initComponents();
    }
    private void initComponents()
    {
        txtInsightTitle.setTextColor(Color.rgb(55,171,152));
    }
    public void selectItem(int option)
    {
        Fragment mFragment = getFragment(option);
        FragmentManager fragmentManager = getSupportFragmentManager();

        fragmentManager.beginTransaction().replace(R.id.principalContent, mFragment).commit();
    }

    public void selectItemBackStack(int option)
    {
        Fragment mFragment = getFragment(option);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.principalContent, mFragment).addToBackStack(null).commit();
    }

    private Fragment getFragment(int option)
    {
        switch (option)
        {
            case 0:
                return new InsightsHome();
            case 1:
                return new ViewInsights();


        }
        return null;
    }


}
