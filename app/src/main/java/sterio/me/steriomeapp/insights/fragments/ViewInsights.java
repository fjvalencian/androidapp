package sterio.me.steriomeapp.insights.fragments;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import sterio.me.adapters.InsightsAdapter;
import sterio.me.customview.CircleGraph;
import sterio.me.customview.CircleGraphLevel;
import sterio.me.object.adapter.InsightsScoreObject;
import sterio.me.steriomeapp.R;

public class ViewInsights extends Fragment
{
    private TextView txtInsightsName;
    private TextView txtTitleInsightsView;
    private TextView txtPorcentaje;
    private TextView txtPorcentajeText;
    private TextView txtExplanationInsights;
    private CircleGraphLevel circleGraphExplanation;
    private ListView lstScoreView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragment_view_insights, container, false);
        initComponents(v);
        return v;
    }


    private void initComponents(View v)
    {

        txtInsightsName = (TextView)v.findViewById(R.id.txtInsightsName);

        txtTitleInsightsView = (TextView)v.findViewById(R.id.txtTitleInsightsView);
        txtPorcentaje = (TextView)v.findViewById(R.id.txtPorcentaje);
        txtExplanationInsights = (TextView)v.findViewById(R.id.txtExplanationInsights);
        txtPorcentajeText = (TextView)v.findViewById(R.id.txtPorcentajeText);
        circleGraphExplanation = (CircleGraphLevel)v.findViewById(R.id.circleGraphExplanation);
        lstScoreView = (ListView)v.findViewById(R.id.lstScoreView);



        circleGraphExplanation.setPercentage(85);
        circleGraphExplanation.setBackgroundColor(Color.WHITE);
        circleGraphExplanation.setBorderColor(getResources().getColor(R.color.elephant));
        circleGraphExplanation.setLevelColor(getResources().getColor(R.color.confetti));

        lstScoreView.setAdapter(new InsightsAdapter(getListObject(),getActivity()));



        putTypography( txtInsightsName, txtTitleInsightsView, txtPorcentaje, txtExplanationInsights,
                txtPorcentajeText);
    }
    private ArrayList<InsightsScoreObject> getListObject()
    {
        ArrayList<InsightsScoreObject> listObjects = new ArrayList<>();
        listObjects.add(new InsightsScoreObject("Modules completed:", "Your score:",0));
        listObjects.add(new InsightsScoreObject("Math: Calculus","80",1));
        listObjects.add(new InsightsScoreObject("English: Verb","65",1));
        listObjects.add(new InsightsScoreObject("Geography: Oceans","83",1));
        listObjects.add(new InsightsScoreObject("Biology: Reproduction III","21",1));
        listObjects.add(new InsightsScoreObject("Physics: Velocity II","44",1));

        return listObjects;
    }
    private void putTypography(TextView... texts)
    {
        Typeface myTypeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand_Bold.otf");

        for(TextView text : texts)
        {
            text.setTypeface(myTypeface);
        }

    }



}
