package sterio.me.steriomeapp;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import sterio.me.controladoras.web_controladora.LoginController;
import sterio.me.util.PutFonts;
import sterio.me.util.Validations;


public class LoginActivity extends Activity implements View.OnClickListener
{
    private TextView txtFirstLine;
    private TextView txtSecondLine;
    private TextView txtCreateAccount;
    private TextView txtForgotPassword;
    private TextView txtErrorMessageMail;
    private TextView txtErrorMessagePassword;
    private EditText edtUser;
    private EditText edtPass;
    private Button btnLogin;
    private Dialog dialog;
    private boolean isCorrectUser;
    private boolean isCorrectPass;
    private int cont = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initComponents();
    }

    private void initComponents()
    {
        isCorrectPass = true;
        isCorrectUser = true;

        txtFirstLine = (TextView)findViewById(R.id.txtFirstLineLogin);
        txtSecondLine = (TextView)findViewById(R.id.txtSecondLineLogin);
        txtCreateAccount = (TextView)findViewById(R.id.txtCreateAccount);
        txtForgotPassword = (TextView)findViewById(R.id.txtForgotPassword);
        txtErrorMessageMail = (TextView)findViewById(R.id.txtErrorMessageMail);
        txtErrorMessagePassword = (TextView)findViewById(R.id.txtBadLearnerCode);
        edtUser = (EditText)findViewById(R.id.edtUser);
        edtPass = (EditText)findViewById(R.id.edtPass);
        btnLogin = (Button)findViewById(R.id.btnLogin);
        putFont();


        btnLogin.setOnClickListener(this);
        txtCreateAccount.setOnClickListener(this);
    }

    private void putFont()
    {
        PutFonts fonts = new PutFonts(this);

        fonts.putFonts(txtFirstLine, txtSecondLine, txtCreateAccount, txtForgotPassword, txtErrorMessageMail,
                txtErrorMessagePassword);
        fonts.putFonts(edtUser,edtPass);
        fonts.putFonts(btnLogin);
    }


    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.btnLogin:
                Validations validation = new Validations(this);
                if(validateUser(validation) & validatePass(validation))
                {
                    new AsyncLogin().execute(edtUser.getText().toString(), edtPass.getText().toString());
                }
                else
                {
                    cont++;
                    if(cont==2)
                    {
                        cont = 0;
                        createLoginDialog();
                    }
                }

                break;
            case R.id.txtCreateAccount:
                Intent i = new Intent(this,SingUp.class);
                startActivity(i);
                finish();
                break;
            case R.id.btnTryAgainLoginDialog:
                dialog.dismiss();
                break;
            case R.id.btnForgotEmailLoginDialog:
                dialog.dismiss();
                break;
            case R.id.btnForgotPasswordLoginDialog:
                dialog.dismiss();
                break;
        }
    }

    private boolean validatePass(Validations validation)
    {
        if(validation.validateTextAndNumber(edtPass.getText().toString()))
        {
            validation.goodAnswer(txtErrorMessagePassword,edtPass);
            isCorrectPass = true;
            return true;
        }
        validation.badAnswer(txtErrorMessagePassword,edtPass);
        isCorrectPass = false;
        return false;
    }

    private boolean validateUser(Validations validation)
    {
        if(validation.validateMail(edtUser.getText().toString()))
        {
            validation.goodAnswer(txtErrorMessageMail,edtUser);
            isCorrectUser = true;
            return true;
        }
        validation.badAnswer(txtErrorMessageMail,edtUser);
        isCorrectUser = true;
        return false;
    }

    private void createLogin()
    {
        Intent i = new Intent(this,CreateProfile.class);
        startActivity(i);
        finish();
    }
    private class AsyncLogin extends AsyncTask<String,Void,Boolean>
    {
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(String... params)
        {
            LoginController login = new LoginController();
            return login.isLoginOk(params[0],params[1]);

        }

        @Override
        protected void onPostExecute(Boolean aBoolean)
        {
            super.onPostExecute(aBoolean);
            if(aBoolean)
            {
                createLogin();
            }
        }

    }
    private void createLoginDialog()
    {
        PutFonts putFonts = new PutFonts(this);
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationZoom;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView txtTitleLoginDialog = (TextView)dialog.findViewById(R.id.txtTitleLoginDialog);
        Button btnTryAgainLoginDialog = (Button)dialog.findViewById(R.id.btnTryAgainLoginDialog);
        Button btnForgotEmailLoginDialog = (Button)dialog.findViewById(R.id.btnForgotEmailLoginDialog);
        Button btnForgotPasswordLoginDialog = (Button)dialog.findViewById(R.id.btnForgotPasswordLoginDialog);

        putFonts.putFonts(txtTitleLoginDialog);
        putFonts.putFonts(btnForgotEmailLoginDialog,btnTryAgainLoginDialog,btnForgotPasswordLoginDialog);

        btnTryAgainLoginDialog.setOnClickListener(this);
        btnForgotEmailLoginDialog.setOnClickListener(this);
        btnForgotPasswordLoginDialog.setOnClickListener(this);

        dialog.show();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putBoolean("isCorrectUser", isCorrectUser);
        outState.putBoolean("isCorrectPass",isCorrectPass);
        outState.putString("textUser", edtUser.getText().toString());
        outState.putString("textPass", edtPass.getText().toString());

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);

        edtUser.setText(savedInstanceState.getString("textUser"));
        edtPass.setText(savedInstanceState.getString("textPass"));
        Validations validations = new Validations(this);
        if(!savedInstanceState.getBoolean("isCorrectUser"))
        {
            validateUser(validations);
        }
        if(!savedInstanceState.getBoolean("isCorrectPass"))
        {
            validatePass(validations);
        }
    }
}
