package sterio.me.steriomeapp;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.File;

import sterio.me.adapters.CustomSpinnerAdapter;
import sterio.me.steriomeapp.dashboard.activities.DashboardActivity;
import sterio.me.util.ImagesUtil;
import sterio.me.util.PutFonts;
import sterio.me.util.UtilFiles;
import sterio.me.util.Validations;


public class CreateProfile extends Activity implements View.OnClickListener
{

    private static final int LOAD_FROM_GALLERY = 0;
    private static final int LOAD_FROM_CAMERA = 1;

    private TextView txtTitle;
    private TextView txtBadAgeSpinner;
    private TextView txtBadGender;
    private TextView txtBadFormNumber;
    private TextView txtBadFormLetter;
    private TextView txtTakePicture;
    private TextView txtNeedHelp;
    private TextView txtNameSchool;
    private Spinner spAge;
    private RelativeLayout layoutImage;
    private RelativeLayout layoutMessage;
    private Spinner spFromNumber;
    private Spinner spFromLetter;
    private Spinner spGender;
    private Button btnComplete;
    private Dialog dialog;
    private Bitmap imgBackground;
    private boolean isClicked;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_profile);
        initComponents();

    }

    private void initComponents()
    {
        txtTitle = (TextView)findViewById(R.id.txtHelloText);
        txtTakePicture = (TextView)findViewById(R.id.txtUploadOrTakePicture);
        txtNeedHelp = (TextView)findViewById(R.id.txtHelp);
        txtNameSchool = (TextView)findViewById(R.id.txtNameSchool);
        txtBadAgeSpinner = (TextView) findViewById(R.id.txtBadAgeSpinner);
        txtBadGender = (TextView) findViewById(R.id.txtBadGender);
        txtBadFormNumber = (TextView) findViewById(R.id.txtBadFormNumber);
        txtBadFormLetter = (TextView)findViewById(R.id.txtBadFormLetter);
        spAge = (Spinner)findViewById(R.id.spAge);
        spFromNumber = (Spinner)findViewById(R.id.spFormNumber);
        spFromLetter = (Spinner)findViewById(R.id.spFormLetter);
        spGender = (Spinner)findViewById(R.id.spGender);
        btnComplete = (Button)findViewById(R.id.btnCompleteProfile);
        layoutImage = (RelativeLayout)findViewById(R.id.layoutImage);
        layoutMessage = (RelativeLayout)findViewById(R.id.layoutMessage);
        putFont();

        spGender.setSelection(1);
        spAge.setSelection(7);
        spFromLetter.setSelection(1);
        spFromNumber.setSelection(7);

        btnComplete.setOnClickListener(this);
        layoutImage.setOnClickListener(this);
    }

    private void putFont()
    {
        PutFonts putFonts = new PutFonts(this);

        putFonts.putFonts(txtTitle,txtTakePicture, txtNeedHelp, txtNameSchool, txtBadAgeSpinner,
                            txtBadGender, txtBadFormNumber,txtBadFormLetter);
        putFonts.putFonts(btnComplete);

        spGender.setAdapter( new CustomSpinnerAdapter(this,R.layout.row_spiner,getGenders()));
        spFromNumber.setAdapter(new CustomSpinnerAdapter(this, R.layout.row_spiner, getFormNumber()));
        spFromLetter.setAdapter(new CustomSpinnerAdapter(this, R.layout.row_spiner, getFormLetter()));
        spAge.setAdapter( new CustomSpinnerAdapter(this,R.layout.row_spiner,getAges()));
    }

    private String[] getFormNumber()
    {
        return new String[]{"Grade","1","2","3","4","5","6","7","8"};
    }
    private String[] getFormLetter()
    {
        return new String[]{"Class","A","B"};
    }

    private String[] getAges()
    {
        return new String[]{"Age","10","11","12","13","14","15","16","17","18","19","20"};
    }

    private String[] getGenders()
    {
        return new String[]{"Gender","Male","Female"};
    }

    @Override
    public void onClick(View v)
    {
        Intent i;
        switch (v.getId())
        {
            case R.id.btnCompleteProfile:
                Validations validation = new Validations(this);

                if(validateAge(validation)&validateSex(validation))
                {
                    layoutMessage.setVisibility(View.GONE);
                   if(validateLetter(validation)&validateNumber(validation))
                   {
                       i = new Intent(this, DashboardActivity.class);
                       startActivity(i);
                       if (isClicked)
                       {
                           UtilFiles utilFiles = new UtilFiles();
                           utilFiles.saveImage(imgBackground);
                       }
                       finish();
                   }
                }
                else if(!validateAge(validation)||!validateSex(validation))
                {
                    validateLetter(validation);
                    validateNumber(validation);
                    layoutMessage.setVisibility(View.VISIBLE);
                }

                break;
            case R.id.layoutImage:

                createDialog();
                break;
            case R.id.btnPhotoLibrary:
                i = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(i, LOAD_FROM_GALLERY);
                dialog.dismiss();
                break;
            case R.id.btnTakeFromCamera:

                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                String root = Environment.getExternalStorageDirectory().toString();
                File file = new File(root + "/.steriome/profile_picture"+File.separator+"image_parser.jpg");
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));

                if (intent.resolveActivity(getPackageManager()) != null)
                {
                    startActivityForResult(intent, LOAD_FROM_CAMERA);
                }
                dialog.dismiss();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        ImagesUtil imagesUtil = new ImagesUtil();
        if (resultCode == RESULT_OK)
        {
            isClicked = true;

            if(requestCode == LOAD_FROM_CAMERA)
            {
                imgBackground = imagesUtil.getImageFromCamera();
            }
            if(requestCode == LOAD_FROM_GALLERY)
            {
                imgBackground = imagesUtil.getImageFromGallery(data, getContentResolver());
            }
            layoutImage.findViewById(R.id.txtUploadOrTakePicture).setVisibility(View.GONE);
            imgBackground = imagesUtil.getRoundedRectBitmap(imgBackground, requestCode, layoutImage,this);
            layoutImage.setBackgroundDrawable(new BitmapDrawable(getResources(),imgBackground));

        }
    }


    private void createDialog()
    {
        Typeface myTypeface = Typeface.createFromAsset(getAssets(), "fonts/Quicksand_Bold.otf");
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_pick_picture);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationZoom;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        ((TextView)dialog.findViewById(R.id.txtTitleDialogPickPicture)).setTypeface(myTypeface);
        ((Button)dialog.findViewById(R.id.btnPhotoLibrary)).setTypeface(myTypeface);
        ((Button)dialog.findViewById(R.id.btnTakeFromCamera)).setTypeface(myTypeface);
        dialog.findViewById(R.id.btnPhotoLibrary).setOnClickListener(this);
        dialog.findViewById(R.id.btnTakeFromCamera).setOnClickListener(this);
        dialog.show();

    }
    private boolean validateAge(Validations validation)
    {
        if(spAge.getSelectedItemPosition() == 0)
        {
            validation.badAnswer(txtBadAgeSpinner,spAge);
            return false;
        }
        validation.goodAnswer(txtBadAgeSpinner,spAge);
        return true;
    }

    private boolean validateSex(Validations validation)
    {
        if(spGender.getSelectedItemPosition() == 0)
        {
            validation.badAnswer(txtBadGender, spGender);
            return false;
        }
        validation.goodAnswer(txtBadGender, spGender);
        return true;
    }

    private boolean validateNumber(Validations validation)
    {
        if(spFromNumber.getSelectedItemPosition() != 0)
        {
            validation.goodAnswer(txtBadFormNumber, spFromNumber);
            return true;
        }
        validation.badAnswer(txtBadFormNumber, spFromNumber);
        return false;
    }
    private boolean validateLetter(Validations validation)
    {
        if(spFromLetter.getSelectedItemPosition() != 0)
        {
            validation.goodAnswer(txtBadFormLetter, spFromLetter);
            return true;
        }
        validation.badAnswer(txtBadFormLetter, spFromLetter);
        return false;
    }
}
