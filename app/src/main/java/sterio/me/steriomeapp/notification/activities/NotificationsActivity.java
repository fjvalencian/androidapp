package sterio.me.steriomeapp.notification.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import sterio.me.base.ActivityBase;
import sterio.me.steriomeapp.R;
import sterio.me.steriomeapp.learning.fragments.CongratulationsFragment;
import sterio.me.steriomeapp.learning.fragments.ModuleCompleteFragment;
import sterio.me.steriomeapp.learning.fragments.QuestionFragment;
import sterio.me.steriomeapp.learning.fragments.StartLearningCourseFragment;
import sterio.me.steriomeapp.learning.fragments.StartLearningFragment;
import sterio.me.steriomeapp.notification.fragments.NotificationFragment;
import sterio.me.steriomeapp.notification.fragments.TeacherProfileFragment;

public class NotificationsActivity extends ActivityBase
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(savedInstanceState==null)
        {
            selectItem(0);
        }
        initComponents();
    }
    private void initComponents()
    {
        txtNotificationsTitle.setTextColor(Color.rgb(240, 236, 100));
    }



    public void selectItem(int option)
    {
        Fragment mFragment = getFragment(option);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.principalContent, mFragment).commit();
    }
    public void selectItemBackStack(int option)
    {
        Fragment mFragment = getFragment(option);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.principalContent, mFragment).addToBackStack(null).commit();
    }

    @Override
    protected void onPause()
    {
        super.onPause();
    }

    private Fragment getFragment(int option)
    {
        switch (option)
        {
            case 0:
                return new NotificationFragment();
            case 1:
                return new TeacherProfileFragment();

        }
        return null;
    }




}
