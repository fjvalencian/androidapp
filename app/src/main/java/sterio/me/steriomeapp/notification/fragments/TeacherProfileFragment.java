package sterio.me.steriomeapp.notification.fragments;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import sterio.me.steriomeapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TeacherProfileFragment extends Fragment
{
    private TextView txtProfileTeacherTitle;
    private TextView txtSchoolTeacherProfile;
    private TextView txtNameTeacherSchool;
    private TextView txtFormTeacherProfile;
    private TextView txtFormValueTeacherProfile;
    private TextView txtSubjects;
    private TextView txtSubjectsValues;
    private TextView txtEmailTeacherProfile;
    private TextView txtEmailValueTeacherProfile;
    private TextView txtPhoneNumberTeacherProfile;
    private TextView txtPhoneNumberValueTeacherProfile;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragment_teacher_profile, container, false);
        initComponent(v);
        return v;
    }

    private void initComponent(View v)
    {
        txtProfileTeacherTitle = (TextView)v.findViewById(R.id.txtProfileTeacherTitle);
        txtSchoolTeacherProfile = (TextView)v.findViewById(R.id.txtSchoolTeacherProfile);
        txtNameTeacherSchool = (TextView)v.findViewById(R.id.txtNameTeacherSchool);
        txtFormTeacherProfile = (TextView)v.findViewById(R.id.txtFormTeacherProfile);
        txtFormValueTeacherProfile = (TextView)v.findViewById(R.id.txtFormValueTeacherProfile);
        txtSubjects = (TextView)v.findViewById(R.id.txtSubjects);
        txtSubjectsValues = (TextView)v.findViewById(R.id.txtSubjectsValues);
        txtEmailTeacherProfile = (TextView)v.findViewById(R.id.txtEmailTeacherProfile);
        txtEmailValueTeacherProfile = (TextView)v.findViewById(R.id.txtEmailValueTeacherProfile);
        txtPhoneNumberTeacherProfile = (TextView)v.findViewById(R.id.txtPhoneNumberTeacherProfile);
        txtPhoneNumberValueTeacherProfile = (TextView)v.findViewById(R.id.txtPhoneNumberValueTeacherProfile);

        putTypography();
    }

    private void putTypography()
    {
        Typeface myTypeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand_Bold.otf");
        Typeface myTypeface2 = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand_Book.otf");

        txtProfileTeacherTitle.setTypeface(myTypeface);
        txtSchoolTeacherProfile.setTypeface(myTypeface);
        txtFormTeacherProfile.setTypeface(myTypeface);
        txtSubjects.setTypeface(myTypeface);
        txtEmailTeacherProfile.setTypeface(myTypeface);
        txtPhoneNumberTeacherProfile.setTypeface(myTypeface);

        txtNameTeacherSchool.setTypeface(myTypeface2);
        txtFormValueTeacherProfile.setTypeface(myTypeface2);
        txtSubjectsValues.setTypeface(myTypeface2);
        txtEmailValueTeacherProfile.setTypeface(myTypeface2);
        txtPhoneNumberValueTeacherProfile.setTypeface(myTypeface2);

    }


}
