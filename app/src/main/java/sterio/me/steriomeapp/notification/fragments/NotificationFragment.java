package sterio.me.steriomeapp.notification.fragments;


import android.app.Dialog;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import sterio.me.steriomeapp.R;
import sterio.me.steriomeapp.notification.activities.NotificationsActivity;


public class NotificationFragment extends Fragment implements View.OnClickListener
{
    private TextView txtTitleNotification;
    private TextView txtAmountNewNotification;
    private TextView txtTextNotification1;
    private TextView txtTextNotification2;
    private TextView txtTextNotification3;
    private TextView txtTextNotification4;
    private TextView txtTextNotification5;
    private TextView txtNew1;
    private TextView txtNew2;
    private TextView txtNew3;
    private TextView txtNew4;
    private TextView txtNew5;
    private TextView txtTitlePastNotification;
    private TextView txtPastNotification1;
    private Dialog dialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragment_notification, container, false);
        initComponents(v);
        return v;
    }

    private void initComponents(View v)
    {
        txtTitleNotification = (TextView)v.findViewById(R.id.txtTitleNotification);
        txtAmountNewNotification = (TextView)v.findViewById(R.id.txtAmountNewNotification);
        txtTextNotification1 = (TextView)v.findViewById(R.id.txtTextNotification1);
        txtTextNotification2 = (TextView)v.findViewById(R.id.txtTextNotification2);
        txtTextNotification3 = (TextView)v.findViewById(R.id.txtTextNotification3);
        txtTextNotification4 = (TextView)v.findViewById(R.id.txtTextNotification4);
        txtTextNotification5 = (TextView)v.findViewById(R.id.txtTextNotification5);
        txtNew1 = (TextView)v.findViewById(R.id.txtNew1);
        txtNew2 = (TextView)v.findViewById(R.id.txtNew2);
        txtNew3 = (TextView)v.findViewById(R.id.txtNew3);
        txtNew4 = (TextView)v.findViewById(R.id.txtNew4);
        txtNew5 = (TextView)v.findViewById(R.id.txtNew5);
        txtTitlePastNotification = (TextView)v.findViewById(R.id.txtTitlePastNotification);
        txtPastNotification1 = (TextView)v.findViewById(R.id.txtPastNotification1);

        v.findViewById(R.id.layoutLargMeeting).setOnClickListener(this);
        v.findViewById(R.id.layoutMeetingSimpleNew).setOnClickListener(this);
        v.findViewById(R.id.layoutMeetingSimple).setOnClickListener(this);
        putTypography();
    }

    private void putTypography()
    {
        Typeface myTypeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand_Bold.otf");
        txtTitleNotification.setTypeface(myTypeface);
        txtAmountNewNotification.setTypeface(myTypeface);
        txtTextNotification1.setTypeface(myTypeface);
        txtTextNotification2.setTypeface(myTypeface);
        txtTextNotification3.setTypeface(myTypeface);
        txtTextNotification4.setTypeface(myTypeface);
        txtTextNotification5.setTypeface(myTypeface);
        txtNew1.setTypeface(myTypeface);
        txtNew2.setTypeface(myTypeface);
        txtNew3.setTypeface(myTypeface);
        txtNew4.setTypeface(myTypeface);
        txtNew5.setTypeface(myTypeface);
        txtTitlePastNotification.setTypeface(myTypeface);
        txtPastNotification1.setTypeface(myTypeface);
    }


    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.layoutLargMeeting:
                createDialogMeeting();
                break;
            case R.id.layoutMeetingSimpleNew:
            case R.id.layoutMeetingSimple:
                createDialogSimple();
                break;
            case R.id.btnClose:
                dialog.dismiss();
                break;
            case R.id.btnTeacherProfile:
                dialog.dismiss();
                ((NotificationsActivity)getActivity()).selectItemBackStack(1);
                break;
        }
    }

    private void createDialogSimple()
    {
        Typeface myTypeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand_Bold.otf");

        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_notification_simple);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationZoom;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        ((TextView)dialog.findViewById(R.id.txtTitle)).setTypeface(myTypeface);
        ((TextView)dialog.findViewById(R.id.txtSchool)).setTypeface(myTypeface);
        ((Button)dialog.findViewById(R.id.btnClose)).setTypeface(myTypeface);
        dialog.findViewById(R.id.btnClose).setOnClickListener(this);
        dialog.show();
    }

    private void createDialogMeeting()
    {
        Typeface myTypeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand_Bold.otf");
        Typeface myTypeface2 = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand_Book.otf");

        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_notification_button);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationZoom;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        ((TextView)dialog.findViewById(R.id.txtTitle)).setTypeface(myTypeface);
        ((TextView)dialog.findViewById(R.id.txtLocation)).setTypeface(myTypeface);
        ((TextView)dialog.findViewById(R.id.txtTimeMeeting)).setTypeface(myTypeface);
        ((TextView)dialog.findViewById(R.id.txtDateMeeting)).setTypeface(myTypeface);
        ((TextView)dialog.findViewById(R.id.txtAssigned)).setTypeface(myTypeface);
        ((Button)dialog.findViewById(R.id.btnTeacherProfile)).setTypeface(myTypeface);
        ((TextView)dialog.findViewById(R.id.txtPlaceMeeting)).setTypeface(myTypeface2);
        ((TextView)dialog.findViewById(R.id.txtTimeMeetingValue)).setTypeface(myTypeface2);
        ((TextView)dialog.findViewById(R.id.txtDateMeetingValue)).setTypeface(myTypeface2);
        ((TextView)dialog.findViewById(R.id.txtAssignedValue)).setTypeface(myTypeface2);


        dialog.findViewById(R.id.btnTeacherProfile).setOnClickListener(this);

        dialog.show();
    }
}
