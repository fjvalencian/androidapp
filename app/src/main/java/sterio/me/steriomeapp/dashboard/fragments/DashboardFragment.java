package sterio.me.steriomeapp.dashboard.fragments;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;

import sterio.me.base.ActivityBase;
import sterio.me.steriomeapp.insights.activities.InsightsActivity;
import sterio.me.steriomeapp.learning.activities.LearningActivity;
import sterio.me.steriomeapp.R;
import sterio.me.steriomeapp.notification.activities.NotificationsActivity;
import sterio.me.steriomeapp.profile.activities.ProfileActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardFragment extends Fragment implements View.OnClickListener
{

    private TextView txtDashboardTitle;
    private TextView txtStartLearning;
    private TextView txtStartLearningNumber;
    private TextView txtViewNotifications;
    private TextView txtViewNotificationsNumber;
    private TextView txtViewInsights;
    private TextView txtViewInsightsNumber;
    private TextView txtYourProfile;
    private TextView txtYourProfileNumber;
    private TextView txtLastOnlineValue;
    private TextView txtLastOnline;
    private TextView txtTimeSpendValue;
    private TextView txtTimeSpend;
    private TextView txtHomeWorkValue;
    private TextView txtHomeWork;
    private ImageView imgProfile;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragment_dashboard, container, false);
        initComponents(v);

        return v;
    }

    @Override
    public void onResume()
    {
        super.onResume();
        ((ActivityBase)getActivity()).searchImageProfile(imgProfile);
    }

    private void initComponents(View v)
    {
        txtDashboardTitle = (TextView)v.findViewById(R.id.txtDashboardTitle);
        txtStartLearning = (TextView)v.findViewById(R.id.txtStartLearning);
        txtStartLearningNumber = (TextView)v.findViewById(R.id.txtStartLearningNumber);
        txtViewNotifications = (TextView)v.findViewById(R.id.txtViewNotifications );
        txtViewNotificationsNumber = (TextView)v.findViewById(R.id.txtViewNotificationsNumber);
        txtViewInsights = (TextView)v.findViewById(R.id.txtViewInsights);
        txtViewInsightsNumber = (TextView)v.findViewById(R.id.txtViewInsightsNumber);
        txtYourProfile = (TextView)v.findViewById(R.id.txtYourProfile);
        txtYourProfileNumber = (TextView)v.findViewById(R.id.txtYourProfileNumber);
        txtLastOnline = (TextView)v.findViewById(R.id.txtLastOnline);
        txtLastOnlineValue = (TextView)v.findViewById(R.id.txtLastOnlineValue);
        txtTimeSpend = (TextView)v.findViewById(R.id.txtTimeSpend);
        txtTimeSpendValue = (TextView)v.findViewById(R.id.txtTimeSpendValue);
        txtHomeWorkValue = (TextView)v.findViewById(R.id.txtHomeWorkValue);
        txtHomeWork = (TextView)v.findViewById(R.id.txtHomeWork);
        imgProfile = (ImageView)v.findViewById(R.id.imgProfile);

        v.findViewById(R.id.rlyStartLearning).setOnClickListener(this);
        v.findViewById(R.id.layoutViewInsights).setOnClickListener(this);
        v.findViewById(R.id.rlyNotification).setOnClickListener(this);
        v.findViewById(R.id.layoutViewProfile).setOnClickListener(this);
        putTypography();
    }

    private void putTypography()
    {
        Typeface myTypeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand_Bold.otf");

        txtDashboardTitle.setTypeface(myTypeface);
        txtStartLearning.setTypeface(myTypeface);
        txtStartLearningNumber.setTypeface(myTypeface);
        txtViewNotifications.setTypeface(myTypeface);
        txtViewNotificationsNumber.setTypeface(myTypeface);
        txtViewInsights.setTypeface(myTypeface);
        txtViewInsightsNumber.setTypeface(myTypeface);
        txtYourProfile.setTypeface(myTypeface);
        txtYourProfileNumber.setTypeface(myTypeface);
        txtLastOnline.setTypeface(myTypeface);
        txtLastOnlineValue.setTypeface(myTypeface);
        txtTimeSpend.setTypeface(myTypeface);
        txtTimeSpendValue.setTypeface(myTypeface);
        txtHomeWorkValue.setTypeface(myTypeface);
        txtHomeWork.setTypeface(myTypeface);
    }


    @Override
    public void onClick(View v)
    {
        Intent i;
        switch (v.getId())
        {
            case R.id.rlyStartLearning:
                i = new Intent(getActivity(), LearningActivity.class);
                startActivity(i);
                break;
            case R.id.layoutViewInsights:
                i = new Intent(getActivity(), InsightsActivity.class);
                startActivity(i);
                break;
            case R.id.rlyNotification:
                i = new Intent(getActivity(), NotificationsActivity.class);
                startActivity(i);
                break;
            case R.id.layoutViewProfile:
                i = new Intent(getActivity(), ProfileActivity.class);
                startActivity(i);
                break;

        }
    }
}
