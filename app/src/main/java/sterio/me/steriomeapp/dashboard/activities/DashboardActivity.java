package sterio.me.steriomeapp.dashboard.activities;


import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import sterio.me.base.ActivityBase;
import sterio.me.customview.CircleView;
import sterio.me.steriomeapp.dashboard.fragments.DashboardFragment;
import sterio.me.steriomeapp.insights.activities.InsightsActivity;
import sterio.me.steriomeapp.learning.activities.LearningActivity;
import sterio.me.steriomeapp.R;
import sterio.me.steriomeapp.notification.activities.NotificationsActivity;


public class DashboardActivity extends ActivityBase implements View.OnClickListener
{
    private static final int CODE_QUESTIONS = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(savedInstanceState==null)
        {
            selectItem(0);
        }
        initComponents();
    }
    private void initComponents()
    {
        txtProfileTitle.setTextColor(Color.rgb(232,205,92));
    }



    private void selectItem(int option)
    {
        Fragment mFragment = getFragment(option);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.principalContent, mFragment).commit();
    }
    public void selectItemBackStack(int option)
    {
        Fragment mFragment = getFragment(option);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.principalContent, mFragment).addToBackStack(null).commit();
    }

    private Fragment getFragment(int option)
    {
        switch (option)
        {
            case 0:
                return new DashboardFragment();

                
        }
        return null;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode== RESULT_OK && resultCode == CODE_QUESTIONS)
        {
            String datos = data.getStringExtra("MESSAGE");
            if(datos.compareToIgnoreCase("PASS")==0)
            {
                selectItemBackStack(3);
            }
        }
    }
    @Override
    public void onClick(View v)
    {
        Intent i;
        switch (v.getId())
        {
            case R.id.layoutLearningTitle:
                i = new Intent(this, LearningActivity.class);
                startActivity(i);
                break;
            case R.id.layoutInsinghts:
                i = new Intent(this, InsightsActivity.class);
                startActivity(i);
                break;
            case R.id.layoutProfile:

                break;
            case R.id.layoutNotifications:
                i = new Intent(this, NotificationsActivity.class);
                startActivity(i);
                break;
        }
    }


}
