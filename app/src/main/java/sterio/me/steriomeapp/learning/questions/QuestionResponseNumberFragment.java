package sterio.me.steriomeapp.learning.questions;


import android.app.Dialog;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import sterio.me.steriomeapp.learning.activities.LearningActivity;
import sterio.me.steriomeapp.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class QuestionResponseNumberFragment extends Fragment implements View.OnClickListener
{
    private TextView txtQuestionPartNumber1;
    private TextView txtQuestionPartNumber2;
    private EditText edtResponseNumber;
    private Button btnSubmitNumber;
    private Dialog dialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragment_question_response_number, container, false);
        initComponents(v);
        return v;
    }

    private void initComponents(View v)
    {
        txtQuestionPartNumber1 = (TextView)v.findViewById(R.id.txtQuestionPartNumber1);
        txtQuestionPartNumber2 = (TextView)v.findViewById(R.id.txtQuestionPartNumber2);
        edtResponseNumber = (EditText)v.findViewById(R.id.edtResponseNumber);
        btnSubmitNumber = (Button)v.findViewById(R.id.btnSubmitNumber);

        btnSubmitNumber.setOnClickListener(this);
        
        putTypography();
    }

    private void putTypography()
    {
        Typeface myTypeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand_Bold.otf");
        Typeface myTypeface2 = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand_Book.otf");
        txtQuestionPartNumber1.setTypeface(myTypeface);
        txtQuestionPartNumber2.setTypeface(myTypeface2);
        edtResponseNumber.setTypeface(myTypeface);
        btnSubmitNumber.setTypeface(myTypeface);

    }


    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.btnSubmitNumber:
                if(getAnswer())
                {
                    createGoodDialog();
                }
                else
                {
                    createBadDialog();
                }
                break;
            case R.id.rlvBadAnswer:
            case R.id.rlvGoodAnswer:
                dialog.dismiss();
                goNextQuestions();
                break;
        }
    }
    private void goNextQuestions()
    {

        ((LearningActivity)getActivity()).changeInnerFragment(4);
    }
    private boolean getAnswer()
    {
        if(edtResponseNumber.getText().toString().compareToIgnoreCase("48")==0)
        {
            return true;
        }
        return false;
    }
    private void createGoodDialog()
    {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_good_answer);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationZoom;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.findViewById(R.id.rlvGoodAnswer).setOnClickListener(this);
        ((TextView)dialog.findViewById(R.id.txtWellDone)).setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand_Bold.otf"));
        dialog.show();
    }
    private void createBadDialog()
    {
        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand_Bold.otf");
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_bad_answer);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationZoom;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.findViewById(R.id.rlvBadAnswer).setOnClickListener(this);
        ((TextView)dialog.findViewById(R.id.txtBad)).setTypeface(typeface);
        ((TextView)dialog.findViewById(R.id.txtExplanation)).setTypeface(typeface);
        dialog.show();
    }
}
