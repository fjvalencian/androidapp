package sterio.me.steriomeapp.learning.questions;

import android.app.Dialog;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import sterio.me.base.ActivityBase;
import sterio.me.enumerations.AnswerEnum;
import sterio.me.steriomeapp.learning.activities.LearningActivity;
import sterio.me.steriomeapp.R;


public class QuestionRadioFragment extends Fragment implements View.OnClickListener
{
    private TextView txtPart1Question;
    private TextView txtPart2Question;
    private int numberQuestion;
    private RadioGroup groupRadio;
    private RadioButton rdbOption1;
    private RadioButton rdbOption2;
    private RadioButton rdbOption3;
    private RadioButton rdbOption4;
    private Button btnSubmitRadio;
    private Dialog dialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragment_question_radio, container, false);
        initComponents(v);
        putQuestion();
        return v;
    }

    private void putQuestion()
    {
        txtPart1Question.setText(getTitleQuestion());
        txtPart2Question.setText(getQuestionText());
        rdbOption1.setText(getOption1());
        rdbOption2.setText(getOption2());
        rdbOption3.setText(getOption3());
        rdbOption4.setText(getOption4());
    }

    private String getTitleQuestion()
    {
        String title = "";
        switch (numberQuestion)
        {
            case 0:
                title = "Question 2";
                break;
            case 1:
                title = "Question 3";
                break;
            case 2:
                title = "Question 4";
                break;
            case 3:
                title = "Question 5";
                break;
            case 4:
                title = "Question 6";
                break;
        }


        return title;
    }

    private String getQuestionText()
    {
        String textQuestion = "";
        switch (numberQuestion)
        {
            case 0:
                textQuestion = "\tChoose the correct form of the verb to fill in the blank in following sentence: \n" +
                        "\n" +
                        "My brother often _______ when he has his bath.";
                break;
            case 1:
                textQuestion = "\tChoose the correct form of the verb to fill in the blank in following sentence: \n" +
                        "\n" +
                        "Little children sometimes ______ the dark.";
                break;
            case 2:
                textQuestion = "\tJoin the following pairs of sentences to make a concise single sentence that does not lose the meaning of the originals: \n" +
                        "\n" +
                        "Makara is a tailor. Makara made my suit.";
                break;
            case 3:
                textQuestion = "\tJoin the following pairs of sentences to make a concise single sentence using “whom”: \n" +
                        "\n" +
                        "Maseleka is a famous musician. I talked to you about Maseleka.";
                break;
            case 4:
                textQuestion = "\tChoose the correct conjunction to fill in the blank in the following sentence: \n" +
                        "\n" +
                        "Jill was exhausted, ________  she went to bed.";
                break;
        }
        return textQuestion;
    }

    private String getOption1()
    {
        String option = "";
        switch (numberQuestion)
        {
            case 0:
                option = "\tto sing";
                break;
            case 1:
                option = "\tfears";
                break;
            case 2:
                option = "\tMakara is the tailor who made\n\tmy suit";
                break;
            case 3:
                option = "\tMaseleka is a famous musician,\n\twhom I talked with you about";
                break;
            case 4:
                option = "\tsince";
                break;
        }
        return option;
    }

    private String getOption2()
    {
        String option = "";
        switch (numberQuestion)
        {
            case 0:
                option = "\tsings";
                break;
            case 1:
                option = "\tfearing";
                break;
            case 2:
                option = "\tMakara made my suit, who is\n\tthe tailor";
                break;
            case 3:
                option = "\tI talked to you about Maseleka,\n \twhom is a famous musician";
                break;
            case 4:
                option = "\tfor";
                break;
        }
        return option;
    }

    private String getOption3()
    {
        String option = "";
        switch (numberQuestion)
        {
            case 0:
                option = "\tsing";
                break;
            case 1:
                option = "\tfear";
                break;
            case 2:
                option = "\tWho made my suit, Makara is\n\tthe tailor";
                break;
            case 3:
                option = "\tMaseleka, whom is a famous\n\tmusician, I talked to you about";
                break;
            case 4:
                option = "\tso";
                break;
        }
        return option;
    }

    private String getOption4()
    {

        String option = "";
        switch (numberQuestion)
        {
            case 0:
                option = "\tsang";
                break;
            case 1:
                option = "\tfeared";
                break;
            case 2:
                option = "\tThe tailor is Makara and she made\n\tmy suit";
                break;
            case 3:
                option = "\tWhom I talked to you about is\n \tMaseleka, a famous musician";
                break;
            case 4:
                option = "\talthough";
                break;
        }
        return option;
    }

    private void initComponents(View v)
    {
        txtPart1Question = (TextView)v.findViewById(R.id.txtPart1Question);
        txtPart2Question = (TextView)v.findViewById(R.id.txtPart2Question);
        groupRadio = (RadioGroup)v.findViewById(R.id.groupRadio);
        rdbOption1 = (RadioButton)v.findViewById(R.id.rdbOption1);
        rdbOption2 = (RadioButton)v.findViewById(R.id.rdbOption2);
        rdbOption3 = (RadioButton)v.findViewById(R.id.rdbOption3);
        rdbOption4 = (RadioButton)v.findViewById(R.id.rdbOption4);
        btnSubmitRadio = (Button)v.findViewById(R.id.btnSubmitRadio);
        btnSubmitRadio.setOnClickListener(this);

        putTypeface();
    }

    private void putTypeface()
    {
        Typeface myTypeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand_Bold.otf");
        Typeface myTypeface2 = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand_Book.otf");
        txtPart1Question.setTypeface(myTypeface);
        txtPart2Question.setTypeface(myTypeface2);
        rdbOption1.setTypeface(myTypeface);
        rdbOption2.setTypeface(myTypeface);
        rdbOption3.setTypeface(myTypeface);
        rdbOption4.setTypeface(myTypeface);
        btnSubmitRadio.setTypeface(myTypeface);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.btnSubmitRadio:
                correctQuestion();
                break;
            case R.id.rlvGoodAnswer:
            case R.id.rlvBadAnswer:
                dialog.dismiss();
                goNextQuestions();
                 break;
            case R.id.layoutSubmitResponse:
                dialog.dismiss();
                break;
        }
    }

    private void correctQuestion()
    {
        int idCheck = groupRadio.getCheckedRadioButtonId();
        boolean flag = false;
        if(idCheck!=-1)
        {
            for(int i = 0;i<groupRadio.getChildCount();i++)
            {
                if(groupRadio.getChildAt(i).getId()==idCheck && i == getGoodAnswer())
                {
                    showDialogGood();
                    flag = true;
                }
            }
            if(!flag)
            {
                showDialogBad();
            }
        }
        else
        {
            showDialogIncomplete();
        }
    }

    private int getGoodAnswer()
    {
        int correctOption = 0;
        switch (numberQuestion)
        {
            case 0:
                correctOption = 1;
                break;
            case 1:
                correctOption = 2;
                break;
            case 2:
                correctOption = 0;
                break;
            case 3:
                correctOption = 1;
                break;
            case 4:
                correctOption = 2;
                break;
        }
        return correctOption;
    }

    private void goNextQuestions()
    {

        if(numberQuestion == 4)
        {
            ((LearningActivity)getActivity()).selectItem(3);
        }
        else
        {
            ((LearningActivity) getActivity()).changeInnerFragment(getGetQuestion() + 2);
        }
    }

    private void showDialogGood()
    {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_good_answer);
        dialog.setCancelable(false);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationZoom;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.findViewById(R.id.rlvGoodAnswer).setOnClickListener(this);
        TextView txtWellDone = (TextView)dialog.findViewById(R.id.txtWellDone);
        TextView txtReason = (TextView)dialog.findViewById(R.id.txtReason);

        ((ActivityBase)getActivity()).putFonts(txtWellDone,txtReason);

        txtReason.setText(getReason(AnswerEnum.GOOD));

        dialog.show();

    }

    private String getReason(AnswerEnum answer)
    {
        switch (answer)
        {
            case GOOD:
                return getGoodReason();
            case BAD:
                return getBadReason();
            case INCOMPLETE:
                break;
        }
        return "";
    }

    private String getBadReason()
    {
        switch (numberQuestion)
        {
            case 0:
                return "The correct answer is:\n Sings. My brother often sings\n when he has his bath.";
            case 1:
                return "The correct answer is:\n Fear. Little children\n sometimes fear the dark.";
            case 2:
                return "The correct answer is:\n Makara is the tailor who\n made my suit.";
            case 3:
                return "The correct answer is:\n I talked to you about\n Maseleka, whom is a famous\n musician";
            case 4:
                return "The correct answer is:\n So. Jill was exhausted,\n so she went to bed.";
        }
        return "";
    }

    private String getGoodReason()
    {
        switch (numberQuestion)
        {
            case 0:
                return "Sings. My brother often sings\n when he has his bath.";
            case 1:
                return "Fear. Little children\nsometimes fear the dark.";
            case 2:
                return "Makara is the tailor who\n made my suit.";
            case 3:
                return "I talked to you about\nMaseleka, whom is a famous\n musician";
            case 4:
                return "So. Jill was exhausted, \nso she went to bed.";
        }
        return "";
    }

    private void showDialogBad()
    {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_bad_answer);
        dialog.setCancelable(false);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationZoom;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.findViewById(R.id.rlvBadAnswer).setOnClickListener(this);
        TextView txtBad = (TextView)dialog.findViewById(R.id.txtBad);
        TextView txtExplanation = (TextView)dialog.findViewById(R.id.txtExplanation);

        ((ActivityBase)getActivity()).putFonts(txtBad,txtExplanation);

        txtExplanation.setText(getReason(AnswerEnum.BAD));

        dialog.show();

    }
    private void showDialogIncomplete()
    {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_submit_answer);
        dialog.setCancelable(false);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationZoom;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.findViewById(R.id.layoutSubmitResponse).setOnClickListener(this);
        TextView txtSubmitVoice = (TextView)dialog.findViewById(R.id.txtSubmitVoice);
        TextView txtSubmitVoicePart2 = (TextView)dialog.findViewById(R.id.txtSubmitVoicePart2);

        ((ActivityBase)getActivity()).putFonts(txtSubmitVoice,txtSubmitVoicePart2);

        txtSubmitVoice.setText("You must select at least  one answer");
        txtSubmitVoicePart2.setText("Try again!");

        dialog.show();

    }

    public int getGetQuestion()
    {
        return numberQuestion;
    }

    public void setGetQuestion(int numberQuestion)
    {
        this.numberQuestion = numberQuestion;
    }


}
