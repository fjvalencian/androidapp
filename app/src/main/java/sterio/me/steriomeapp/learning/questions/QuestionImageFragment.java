package sterio.me.steriomeapp.learning.questions;


import android.app.Dialog;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import sterio.me.steriomeapp.learning.activities.LearningActivity;
import sterio.me.steriomeapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class QuestionImageFragment extends Fragment implements View.OnClickListener
{
    private TextView txtQuestionPartImage1;
    private TextView txtQuestionPartImage2;
    private EditText edtEnterNumber;
    private Button btnSubmitImage;
    private Dialog dialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragment_question_image, container, false);
        initComponents(v);
        return v;
    }

    private void initComponents(View v)
    {
        txtQuestionPartImage1 = (TextView)v.findViewById(R.id.txtQuestionPartImage1);
        txtQuestionPartImage2 = (TextView)v.findViewById(R.id.txtQuestionPartImage2);
        edtEnterNumber = (EditText)v.findViewById(R.id.edtEnterNumber);
        btnSubmitImage = (Button)v.findViewById(R.id.btnSubmitImage);

        putTypography();

        btnSubmitImage.setOnClickListener(this);
    }

    private void putTypography()
    {
        Typeface myTypeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand_Bold.otf");
        Typeface myTypeface2 = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand_Book.otf");

        txtQuestionPartImage1.setTypeface(myTypeface);
        txtQuestionPartImage2.setTypeface(myTypeface2);
        edtEnterNumber.setTypeface(myTypeface);
        btnSubmitImage.setTypeface(myTypeface);
    }


    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.btnSubmitImage:
                if(getCorrectAnswer())
                {
                    createGoodDialog();
                }
                else
                {
                    createBadDialog();
                }
                break;
            case R.id.rlvBadAnswer:
            case R.id.rlvGoodAnswer:
                dialog.dismiss();
                goNextQuestions();
                break;
        }
    }

    private boolean getCorrectAnswer()
    {
        if(edtEnterNumber.getText().toString().compareToIgnoreCase("40") == 0)
        {
            return true;
        }
        return false;
    }
    private void goNextQuestions()
    {

        ((LearningActivity)getActivity()).selectItem(3);
    }

    private void createGoodDialog()
    {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_good_answer);
        dialog.setCancelable(false);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationZoom;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.findViewById(R.id.rlvGoodAnswer).setOnClickListener(this);
        ((TextView)dialog.findViewById(R.id.txtWellDone)).setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand_Bold.otf"));
        dialog.show();
    }
    private void createBadDialog()
    {
        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand_Bold.otf");
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_bad_answer);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationZoom;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.findViewById(R.id.rlvBadAnswer).setOnClickListener(this);
        ((TextView)dialog.findViewById(R.id.txtBad)).setTypeface(typeface);
        TextView textExplanation = (TextView)dialog.findViewById(R.id.txtExplanation);
        textExplanation.setTypeface(typeface);
        textExplanation.setText("The answer is 40, you need to count every square.");
        dialog.show();
    }
}
