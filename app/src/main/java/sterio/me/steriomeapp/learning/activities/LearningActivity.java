package sterio.me.steriomeapp.learning.activities;


import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import sterio.me.base.ActivityBase;
import sterio.me.customview.CircleView;
import sterio.me.interfaces.OnChangeInnerFragment;
import sterio.me.steriomeapp.learning.fragments.CongratulationsFragment;
import sterio.me.steriomeapp.learning.fragments.ModuleCompleteFragment;
import sterio.me.steriomeapp.learning.fragments.QuestionFragment;
import sterio.me.steriomeapp.learning.fragments.StartLearningCourseFragment;
import sterio.me.steriomeapp.learning.fragments.StartLearningFragment;
import sterio.me.steriomeapp.R;


public class LearningActivity extends ActivityBase
{

    private OnChangeInnerFragment listener;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(savedInstanceState==null)
        {
            selectItem(0);
        }
        initComponents();

    }

    private void initComponents()
    {
        txtLearningTitle.setTextColor(Color.rgb(174,207,116));
    }


    public void selectItem(int option)
    {
        Fragment mFragment = getFragment(option);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.principalContent, mFragment).commit();
    }
    public void selectItemBackStack(int option)
    {
        Fragment mFragment = getFragment(option);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.principalContent, mFragment).addToBackStack(null).commit();
    }

    @Override
    protected void onPause()
    {
        super.onPause();
    }

    private Fragment getFragment(int option)
    {
        switch (option)
        {
            case 0:
                return new StartLearningFragment();
            case 1:
                return new StartLearningCourseFragment();
            case 2:
                return new QuestionFragment();
            case 3:
                return new CongratulationsFragment();
            case 4:
                return new ModuleCompleteFragment();
                
        }
        return null;
    }

    public void changeInnerFragment(int position)
    {
        if(listener!= null)
        {
            listener.changeInnerInterface(position);
        }
    }


    public void removeOnChangeInnerFragmentListener()
    {
        this.listener = null;
    }

    public void addChangeInnerFragmentListener(OnChangeInnerFragment listener)
    {
        this.listener = listener;
    }
}
