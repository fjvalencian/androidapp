package sterio.me.steriomeapp.learning.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import sterio.me.base.ActivityBase;
import sterio.me.customview.GraphCircleLine;
import sterio.me.steriomeapp.insights.activities.InsightsActivity;
import sterio.me.steriomeapp.learning.activities.LearningActivity;
import sterio.me.steriomeapp.R;
import sterio.me.util.PutFonts;


public class CongratulationsFragment extends Fragment implements View.OnClickListener
{

    private TextView txtCongratulations;
    private TextView txtScore;
    private TextView txtPercentageUnit;
    private TextView txtPercentageTen;
    private TextView txtPercentageSymbol;
    private Button btnNextModule;
    private Button btnViewInsig;
    private ImageView imgProfileCongratulations;
    private GraphCircleLine graphCircleLine;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragment_congratulations, container, false);
        initComponents(v);
        ((ActivityBase)getActivity()).searchImageProfile(imgProfileCongratulations);
        return v;
    }


    private void initComponents(View v)
    {
        txtScore = (TextView)v.findViewById(R.id.txtScore);
        txtPercentageUnit = (TextView)v.findViewById(R.id.txtPercentageUnit);
        txtCongratulations = (TextView)v.findViewById(R.id.txtCongratulations);
        txtPercentageSymbol = (TextView)v.findViewById(R.id.txtPercentageSymbol);
        txtPercentageTen = (TextView)v.findViewById(R.id.txtPercentageTen);
        btnNextModule = (Button)v.findViewById(R.id.btnNextModule);
        btnViewInsig = (Button)v.findViewById(R.id.btnViewInsig);
        imgProfileCongratulations = (ImageView)v.findViewById(R.id.imgProfileCongratulations);
        graphCircleLine = (GraphCircleLine)v.findViewById(R.id.graphCircleLine);

        graphCircleLine.setValue(96);
        graphCircleLine.setEdtTextScore(txtPercentageUnit);
        graphCircleLine.setEdtTextTen(txtPercentageTen);
        graphCircleLine.setEdtMessage(txtScore);
        graphCircleLine.setBtnNext(btnNextModule);
        graphCircleLine.setBtnInsig(btnViewInsig);
        putTypography();

        btnNextModule.setOnClickListener(this);
        btnViewInsig.setOnClickListener(this);
    }
    private void putTypography()
    {
        PutFonts putFonts = new PutFonts(getActivity());
        putFonts.putFonts(txtScore, txtCongratulations, txtPercentageUnit, txtPercentageSymbol,txtPercentageTen);
        putFonts.putFonts(btnNextModule,btnViewInsig);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.btnNextModule:
                ((LearningActivity)getActivity()).selectItem(4);
                break;
            case R.id.btnViewInsig:
                Intent i = new Intent(getActivity(), InsightsActivity.class);
                startActivity(i);
                getActivity().finish();
                break;
        }
    }


}
