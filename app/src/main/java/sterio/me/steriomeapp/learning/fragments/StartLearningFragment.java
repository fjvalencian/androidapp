package sterio.me.steriomeapp.learning.fragments;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import sterio.me.steriomeapp.learning.activities.LearningActivity;
import sterio.me.steriomeapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class StartLearningFragment extends Fragment implements View.OnClickListener
{

    private TextView txtTitleStartLearning;
    private TextView txtTitleStartLearningNew;
    private TextView txtNewTopic1;
    private TextView txtNewTopic2;
    private TextView txtNewTopic3;
    private Button btnNewCourse1;
    private Button btnNewCourse2;
    private Button btnNewCourse3;
    private Button course1;
    private Button course2;
    private Button course3;
    private Button course4;
    private Button course5;
    private Button course6;
    private Button course7;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragment_start_learning, container, false);
        initComponents(v);
        return v;
    }

    private void initComponents(View v)
    {
        txtTitleStartLearning = (TextView)v.findViewById(R.id.txtTitleStartLearning);
        txtTitleStartLearningNew = (TextView)v.findViewById(R.id.txtTitleStartLearningNew);
        txtNewTopic1 = (TextView)v.findViewById(R.id.txtNewTopic1);
        txtNewTopic2 = (TextView)v.findViewById(R.id.txtNewTopic2);
        txtNewTopic3 = (TextView)v.findViewById(R.id.txtNewTopic3);
        btnNewCourse1 = (Button)v.findViewById(R.id.btnNewCourse1);
        btnNewCourse2 = (Button)v.findViewById(R.id.btnNewCourse2);
        btnNewCourse3 = (Button)v.findViewById(R.id.btnNewCourse3);
        course1 = (Button)v.findViewById(R.id.btnCourse1);
        course2 = (Button)v.findViewById(R.id.btnCourse2);
        course3 = (Button)v.findViewById(R.id.btnCourse3);
        course4 = (Button)v.findViewById(R.id.btnCourse4);
        course5 = (Button)v.findViewById(R.id.btnCourse5);
        course6 = (Button)v.findViewById(R.id.btnCourse6);
        course7 = (Button)v.findViewById(R.id.btnCourse7);

        course1.setOnClickListener(this);
        course2.setOnClickListener(this);
        course3.setOnClickListener(this);
        course4.setOnClickListener(this);
        course5.setOnClickListener(this);
        course6.setOnClickListener(this);
        course7.setOnClickListener(this);
        btnNewCourse1.setOnClickListener(this);
        btnNewCourse2.setOnClickListener(this);
        btnNewCourse3.setOnClickListener(this);

        putTypography();

    }

    private void putTypography()
    {
        Typeface myTypeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand_Bold.otf");

        txtTitleStartLearning.setTypeface(myTypeface);
        txtTitleStartLearningNew.setTypeface(myTypeface);
        course1.setTypeface(myTypeface);
        course2.setTypeface(myTypeface);
        course3.setTypeface(myTypeface);
        course4.setTypeface(myTypeface);
        course5.setTypeface(myTypeface);
        course6.setTypeface(myTypeface);
        course7.setTypeface(myTypeface);
        txtNewTopic1.setTypeface(myTypeface);
        txtNewTopic2.setTypeface(myTypeface);
        txtNewTopic3.setTypeface(myTypeface);
        btnNewCourse1.setTypeface(myTypeface);
        btnNewCourse2.setTypeface(myTypeface);
        btnNewCourse3.setTypeface(myTypeface);
    }


    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.btnCourse1:
                ((LearningActivity)getActivity()).selectItemBackStack(1);
                break;
            case R.id.btnCourse2:
                break;
            case R.id.btnCourse3:
                break;
            case R.id.btnCourse4:
                break;
            case R.id.btnCourse5:
                break;
            case R.id.btnCourse6:
                break;
            case R.id.btnCourse7:
                break;
            case R.id.btnNewCourse1:
                ((LearningActivity)getActivity()).selectItemBackStack(2);
                break;
            case R.id.btnNewCourse2:
                break;
            case R.id.btnNewCourse3:
                break;
        }
    }
}
