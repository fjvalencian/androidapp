package sterio.me.steriomeapp.learning.fragments;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import sterio.me.steriomeapp.learning.activities.LearningActivity;
import sterio.me.steriomeapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class StartLearningCourseFragment extends Fragment implements View.OnClickListener
{
    private TextView txtTitleMath;
    private TextView txtCurrentModule;
    private TextView txtPercentage;
    private TextView txtCompleteModule;
    private Button btnCourse1Inside;
    private Button btnCourse2Inside;
    private Button btnCourse3Inside;
    private Button btnCourse4Inside;
    private Button btnCourse5Inside;
    private Button btnCourse6Inside;
    private ProgressBar skProgress;
    private RelativeLayout layoutContinue;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragment_start_learning_course, container, false);
        initComponents(v);
        return v;
    }

    private void initComponents(View v)
    {
        txtTitleMath = (TextView)v.findViewById(R.id.txtTitleMath);
        txtCurrentModule = (TextView)v.findViewById(R.id.txtCurrentModule);
        txtPercentage = (TextView)v.findViewById(R.id.txtPorcentaje);
        txtCompleteModule = (TextView)v.findViewById(R.id.txtCompleteModule);

        btnCourse1Inside = (Button)v.findViewById(R.id.btnCourse1Inside);
        btnCourse2Inside = (Button)v.findViewById(R.id.btnCourse2Inside);
        btnCourse3Inside = (Button)v.findViewById(R.id.btnCourse3Inside);
        btnCourse4Inside = (Button)v.findViewById(R.id.btnCourse4Inside);
        btnCourse5Inside = (Button)v.findViewById(R.id.btnCourse5Inside);
        btnCourse6Inside = (Button)v.findViewById(R.id.btnCourse6Inside);

        layoutContinue = (RelativeLayout)v.findViewById(R.id.layoutContinue);

        skProgress = (ProgressBar)v.findViewById(R.id.skProgress);

        btnCourse1Inside.setOnClickListener(this);
        btnCourse2Inside.setOnClickListener(this);
        btnCourse3Inside.setOnClickListener(this);
        btnCourse4Inside.setOnClickListener(this);
        btnCourse5Inside.setOnClickListener(this);
        btnCourse6Inside.setOnClickListener(this);
        layoutContinue.setOnClickListener(this);

        putTypography();

    }

    private void putTypography()
    {
        Typeface myTypeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand_Bold.otf");

        txtTitleMath.setTypeface(myTypeface);
        txtCurrentModule.setTypeface(myTypeface);
        txtPercentage.setTypeface(myTypeface);
        txtCompleteModule.setTypeface(myTypeface);

        btnCourse1Inside.setTypeface(myTypeface);
        btnCourse2Inside.setTypeface(myTypeface);
        btnCourse3Inside.setTypeface(myTypeface);
        btnCourse4Inside.setTypeface(myTypeface);
        btnCourse5Inside.setTypeface(myTypeface);
        btnCourse6Inside.setTypeface(myTypeface);
    }


    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.layoutContinue:
                ((LearningActivity)getActivity()).selectItemBackStack(2);

                break;
        }
    }
}
