package sterio.me.steriomeapp.learning.questions;


import android.app.Dialog;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import sterio.me.base.ActivityBase;
import sterio.me.enumerations.AnswerEnum;
import sterio.me.steriomeapp.learning.activities.LearningActivity;
import sterio.me.steriomeapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class QuestionTextFragment extends Fragment implements View.OnClickListener
{
    private TextView txtQuestionPart1;
    private TextView txtQuestionPart2;
    private EditText edtResponse;
    private Button btnSubmit;
    private Dialog dialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragment_quiestion_text, container, false);
        initComponents(v);
        return v;
    }

    private void initComponents(View v)
    {
        txtQuestionPart1 = (TextView)v.findViewById(R.id.txtQuestionPart1);
        txtQuestionPart2 = (TextView)v.findViewById(R.id.txtQuestionPart2);
        edtResponse = (EditText)v.findViewById(R.id.edtResponse);
        btnSubmit = (Button)v.findViewById(R.id.btnSubmit);

        putTypography();
        putQuestion();

        btnSubmit.setOnClickListener(this);

    }

    private void putQuestion()
    {
        txtQuestionPart1.setText("Question 1:");
        txtQuestionPart2.setText("Type in the correct form of the verb to fill in the blank in following sentence:\n" +
                "Go, goes, going, gone\n" +
                " \n" +
                "\n" +
                "Every fortnight cattle _______ to the water tank.");
    }

    private void putTypography()
    {
        Typeface myTypeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand_Bold.otf");
        Typeface myTypeface2 = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand_Book.otf");

        txtQuestionPart1.setTypeface(myTypeface);
        txtQuestionPart2.setTypeface(myTypeface2);
        edtResponse.setTypeface(myTypeface);
        btnSubmit.setTypeface(myTypeface);
    }


    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.btnSubmit:
                correctAnswer();
                break;
            case R.id.rlvGoodAnswer:
            case R.id.rlvBadAnswer:
                dialog.dismiss();
                goNextQuestions();
                break;
            case R.id.layoutSubmitResponse:
                dialog.dismiss();
                break;
        }
    }

    private void correctAnswer()
    {
        if(edtResponse.getText().toString().compareToIgnoreCase("")==0)
        {
            showDialogIncomplete();
        }
        else if(edtResponse.getText().toString().compareToIgnoreCase("")==0)
        {
            showDialogGood();
        }
        else
        {
            showDialogBad();
        }
    }

    private void goNextQuestions()
    {
        ((LearningActivity)getActivity()).changeInnerFragment(1);
    }

    private void showDialogGood()
    {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_good_answer);
        dialog.setCancelable(false);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationZoom;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.findViewById(R.id.rlvGoodAnswer).setOnClickListener(this);
        TextView txtWellDone = (TextView)dialog.findViewById(R.id.txtWellDone);
        TextView txtReason = (TextView)dialog.findViewById(R.id.txtReason);

        ((ActivityBase)getActivity()).putFonts(txtWellDone,txtReason);

        txtReason.setText("Go. Every fortnight cattle \n" +
                "go to the water tank.");

        dialog.show();
    }
    private void showDialogBad()
    {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_bad_answer);
        dialog.setCancelable(false);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationZoom;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.findViewById(R.id.rlvBadAnswer).setOnClickListener(this);
        TextView txtBad = (TextView)dialog.findViewById(R.id.txtBad);
        TextView txtExplanation = (TextView)dialog.findViewById(R.id.txtExplanation);

        ((ActivityBase)getActivity()).putFonts(txtBad,txtExplanation);

        txtExplanation.setText("The correct answer is:\n Go. Every fortnight cattle \n" +
                "go to the water tank.");

        dialog.show();

    }
    private void showDialogIncomplete()
    {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_submit_answer);
        dialog.setCancelable(false);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationZoom;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.findViewById(R.id.layoutSubmitResponse).setOnClickListener(this);
        TextView txtSubmitVoice = (TextView)dialog.findViewById(R.id.txtSubmitVoice);
        TextView txtSubmitVoicePart2 = (TextView)dialog.findViewById(R.id.txtSubmitVoicePart2);

        ((ActivityBase)getActivity()).putFonts(txtSubmitVoice,txtSubmitVoicePart2);

        txtSubmitVoice.setText("You must select at least  one answer");
        txtSubmitVoicePart2.setText("Try again!");

        dialog.show();

    }
}
