package sterio.me.steriomeapp.learning.questions;


import android.app.Dialog;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.IOException;

import sterio.me.steriomeapp.learning.activities.LearningActivity;
import sterio.me.steriomeapp.R;

public class QuestionVoiceFragment extends Fragment implements View.OnClickListener, Animation.AnimationListener
{
    private static final String LOG_TAG = "AudioRecord";
    private TextView txtQuestionPart1;
    private TextView txtQuestionPart2;
    private TextView txtTouchToRecord;
    private RelativeLayout layoutSound;
    private Button btnSubmit;
    private Dialog dialog;
    private ScaleAnimation scaleAnimation;
    private MediaRecorder mRecorder = null;
    private String mFileName = null;

    public QuestionVoiceFragment()
    {
        mFileName = Environment.getExternalStorageDirectory().getAbsolutePath();
        mFileName += "/audioRecordTest.mp3";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragment_question_voice, container, false);
        initComponents(v);
        return v;
    }

    private void initComponents(View v)
    {
        txtQuestionPart1 = (TextView) v.findViewById(R.id.txtQuestionPart1);
        txtQuestionPart2 = (TextView) v.findViewById(R.id.txtQuestionPart2);
        txtTouchToRecord = (TextView)v.findViewById(R.id.txtTouchToRecord);
        layoutSound = (RelativeLayout)v.findViewById(R.id.layoutSound);
        btnSubmit = (Button)v.findViewById(R.id.btnSubmitVoiceAnswerd);

        putTypography();
        layoutSound.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
    }

    private void putTypography()
    {
        Typeface myTypeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand_Bold.otf");
        Typeface myTypeface2 = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand_Book.otf");

        txtQuestionPart1.setTypeface(myTypeface);
        txtQuestionPart2.setTypeface(myTypeface2);
        txtTouchToRecord.setTypeface(myTypeface);
        btnSubmit.setTypeface(myTypeface);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.btnSubmitVoiceAnswerd:
                createDialog();
                break;
            case R.id.layoutSubmitResponse:
                dialog.dismiss();
                goNextQuestion();
                break;
            case R.id.layoutSound:
                initAnimation(v);
                break;

        }
    }
    private void startRecording()
    {

        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mRecorder.setOutputFile(mFileName);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try
        {
            mRecorder.prepare();
            mRecorder.start();
        }
        catch (IOException e)
        {
            Log.e(LOG_TAG, "prepare() failed");
        }


    }
    private void stopRecording()
    {
        mRecorder.stop();
        mRecorder.release();
        mRecorder = null;
    }

    @Override
    public void onPause()
    {
        if(mRecorder!=null)
        {
            stopRecording();
        }
        super.onPause();
    }

    private void initAnimation(View v)
    {

        if(scaleAnimation!=null && v.getAnimation().hasStarted())
        {
            v.setBackgroundResource(R.drawable.circle_sound);
            txtTouchToRecord.setText(R.string.response_voice);
            v.getAnimation().cancel();
            scaleAnimation = null;
            stopRecording();
        }
        else
        {

            startRecording();
            v.setBackgroundResource(R.drawable.circle_recording);
            txtTouchToRecord.setText(R.string.stop_recording_voice);
            scaleAnimation = new ScaleAnimation(1.0f, 0.8f, 1.0f, 0.8f,
                    ScaleAnimation.RELATIVE_TO_SELF, 0.5f,
                    Animation.RELATIVE_TO_SELF, 0.5f);
            scaleAnimation.setDuration(1000);
            scaleAnimation.setRepeatMode(ScaleAnimation.REVERSE);
            scaleAnimation.setRepeatCount(ScaleAnimation.INFINITE);
            scaleAnimation.setAnimationListener(this);
            v.startAnimation(scaleAnimation);
        }

    }

    private void goNextQuestion()
    {
        ((LearningActivity)getActivity()).changeInnerFragment(2);
    }

    private void createDialog()
    {
        Typeface myTypeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand_Bold.otf");
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_submit_answer);
        dialog.setCancelable(false);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationZoom;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.findViewById(R.id.layoutSubmitResponse).setOnClickListener(this);
        ((TextView)dialog.findViewById(R.id.txtSubmitVoice)).setTypeface(myTypeface);
        ((TextView)dialog.findViewById(R.id.txtSubmitVoicePart2)).setTypeface(myTypeface);
        dialog.show();
    }

    @Override
    public void onAnimationStart(Animation animation)
    {

    }

    @Override
    public void onAnimationEnd(Animation animation)
    {
        ScaleAnimation scaleAnimation2 = new ScaleAnimation(1.0f, 1f, 1.0f, 1f,
                ScaleAnimation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        layoutSound.startAnimation(scaleAnimation2);
    }

    @Override
    public void onAnimationRepeat(Animation animation)
    {

    }
}
