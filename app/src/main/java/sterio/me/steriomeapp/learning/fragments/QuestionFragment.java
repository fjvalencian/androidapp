package sterio.me.steriomeapp.learning.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import sterio.me.interfaces.OnChangeInnerFragment;
import sterio.me.steriomeapp.learning.activities.LearningActivity;
import sterio.me.steriomeapp.learning.questions.QuestionImageFragment;
import sterio.me.steriomeapp.learning.questions.QuestionRadioFragment;
import sterio.me.steriomeapp.learning.questions.QuestionResponseNumberFragment;
import sterio.me.steriomeapp.learning.questions.QuestionTextFragment;
import sterio.me.steriomeapp.learning.questions.QuestionVoiceFragment;
import sterio.me.steriomeapp.R;


public class QuestionFragment extends Fragment implements OnChangeInnerFragment
{

    private TextView txtPause;
    private TextView txtSubject;
    private TextView txtSkip;
    private ProgressBar spProgressQuestion;

    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container, Bundle savedInstanceState)
    {
        View v =  inflater.inflate(R.layout.fragment_question, container, false);
        initComponents(v);
        if(savedInstanceState==null)
        {
            selectItem(0);
        }
        return v;

    }



    private void selectItem(int option)
    {
        Fragment mFragment = getFragment(option);
        FragmentManager fragmentManager = getChildFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.principalContentQuestions, mFragment).commit();
    }

    private void initComponents(View v)
    {

        txtPause = (TextView)v.findViewById(R.id.txtPause);
        txtSubject = (TextView)v.findViewById(R.id.txtSubject);
        txtSkip = (TextView)v.findViewById(R.id.txtSkip);
        spProgressQuestion = (ProgressBar)v.findViewById(R.id.spProgressQuestion);
        spProgressQuestion.setMax(6);
        putTypography();
    }

    @Override
    public void onPause()
    {
        ((LearningActivity)getActivity()).removeOnChangeInnerFragmentListener();
        super.onPause();

    }

    @Override
    public void onResume()
    {
        ((LearningActivity)getActivity()).addChangeInnerFragmentListener(this);
        super.onResume();
    }

    private void putTypography()
    {
        Typeface myTypeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand_Bold.otf");

        txtPause.setTypeface(myTypeface);
        txtSubject.setTypeface(myTypeface);
        txtSkip.setTypeface(myTypeface);
    }

    private Fragment getFragment(int option)
    {
        QuestionRadioFragment question;
        switch (option)
        {
            case 0:
                return new QuestionTextFragment();
            case 1:
                spProgressQuestion.setProgress(spProgressQuestion.getProgress()+1);
                question = new QuestionRadioFragment();
                question.setGetQuestion(0);
                return question;
            case 2:
                spProgressQuestion.setProgress(spProgressQuestion.getProgress()+1);
                question = new QuestionRadioFragment();
                question.setGetQuestion(1);
                return question;
            case 3:
                spProgressQuestion.setProgress(spProgressQuestion.getProgress()+1);
                question = new QuestionRadioFragment();
                question.setGetQuestion(2);
                return question;
            case 4:
                spProgressQuestion.setProgress(spProgressQuestion.getProgress()+1);
                question = new QuestionRadioFragment();
                question.setGetQuestion(3);
                return question;
            case 5:
                spProgressQuestion.setProgress(spProgressQuestion.getProgress()+1);
                question = new QuestionRadioFragment();
                question.setGetQuestion(4);
                return question;

        }
        return null;
    }


    @Override
    public void changeInnerInterface(int position)
    {
        Fragment mFragment = getFragment(position);
        FragmentManager fragmentManager = getChildFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.principalContentQuestions, mFragment).addToBackStack(null).commit();
    }
}
