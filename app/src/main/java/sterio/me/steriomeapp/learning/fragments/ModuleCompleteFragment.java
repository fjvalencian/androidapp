package sterio.me.steriomeapp.learning.fragments;


import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import sterio.me.adapters.AnswerAdapter;
import sterio.me.object.adapter.BaseQuestionObject;
import sterio.me.object.adapter.MultipleOptionQuestionObject;
import sterio.me.object.adapter.TextQuestionObject;
import sterio.me.object.adapter.VoiceQuestionObject;
import sterio.me.steriomeapp.insights.activities.InsightsActivity;
import sterio.me.steriomeapp.R;


public class ModuleCompleteFragment extends Fragment implements View.OnClickListener
{
    private TextView txtTitle;
    private TextView txtResults;
    private Button btnRetakeModule;
    private Button btnViewInsight;
    private ListView listResult;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View v =  inflater.inflate(R.layout.fragment_module_complete, container, false);
        initComponents(v);
        return v;
    }

    private void initComponents(View v)
    {
        txtTitle = (TextView)v.findViewById(R.id.txtTitleInsightsHome);
        txtResults = (TextView)v.findViewById(R.id.txtResults);
        btnRetakeModule = (Button)v.findViewById(R.id.btnRetakeModule);
        btnViewInsight = (Button)v.findViewById(R.id.btnViewInsight);
        listResult = (ListView)v.findViewById(R.id.listResult);


        listResult.setAdapter(new AnswerAdapter(getListAnswer(),getActivity()));

        btnViewInsight.setOnClickListener(this);
        
        putTypography();
    }

    private List<BaseQuestionObject> getListAnswer()
    {
        List<BaseQuestionObject> list = new ArrayList();

        list.add(new MultipleOptionQuestionObject("1: Evaluate [2 - (3 - 3(4 - 2) + 2)] + 3",3,2));
        list.add(new VoiceQuestionObject("2: What is the square root of 25?","",true));
        list.add(new TextQuestionObject("3: one positive number is 4 more than anther. the sum" +
                                        "of their squares is 40.\nType the 2 number","2,6",true));

        return list;

    }

    private void putTypography()
    {
        Typeface myTypeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand_Bold.otf");
        txtTitle.setTypeface(myTypeface);
        txtResults.setTypeface(myTypeface);
        btnRetakeModule.setTypeface(myTypeface);
        btnViewInsight.setTypeface(myTypeface);
    }


    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.btnViewInsight:
                Intent i = new Intent(getActivity(), InsightsActivity.class);
                startActivity(i);
                getActivity().finish();
                break;
        }
    }
}
