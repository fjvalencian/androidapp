package sterio.me.steriomeapp.profile.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import sterio.me.base.ActivityBase;
import sterio.me.steriomeapp.R;
import sterio.me.steriomeapp.profile.fragments.ProfileFragment;

public class ProfileActivity extends ActivityBase
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(savedInstanceState==null)
        {
            selectItem(0);
        }
    }



    public void selectItem(int option)
    {
        Fragment mFragment = getFragment(option);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.principalContent, mFragment).commit();
    }
    public void selectItemBackStack(int option)
    {
        Fragment mFragment = getFragment(option);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.principalContent, mFragment).addToBackStack(null).commit();
    }

    @Override
    protected void onPause()
    {
        super.onPause();
    }

    private Fragment getFragment(int option)
    {
        switch (option)
        {
            case 0:
                return new ProfileFragment();

        }
        return null;
    }




}
