package sterio.me.steriomeapp.profile.fragments;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.File;

import sterio.me.base.ActivityBase;
import sterio.me.steriomeapp.LoginActivity;
import sterio.me.steriomeapp.R;
import sterio.me.util.ImagesUtil;
import sterio.me.util.UtilFiles;

public class ProfileFragment extends Fragment implements View.OnClickListener
{

    private static final int LOAD_FROM_GALLERY = 0;
    private static final int LOAD_FROM_CAMERA = 1;
    private TextView txtProfileTitle;
    private TextView txtSchoolProfile;
    private TextView txtNameSchool;
    private TextView txtAgeProfile;
    private TextView txtAgeValueProfile;
    private TextView txtGenderProfile;
    private TextView txtGenderValueProfile;
    private TextView txtFormProfile;
    private TextView txtFormValueProfile;
    private TextView txtEmailProfile;
    private TextView txtPhoneNumberProfile;
    private TextView txtEmailValueProfile;
    private TextView txtPhoneNumberValueProfile;
    private EditText edtEmailValueProfile;
    private EditText edtPhoneNumberValueProfile;
    private Button btnLogout;
    private Button btnSaveInfo;
    private ImageView imgProfile;
    private Dialog dialog;
    private Bitmap imgBackground;
    private boolean isEditable;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragment_profile, container, false);
        initComponents(v);
        ((ActivityBase)getActivity()).searchImageProfile(imgProfile);
        return v;
    }

    private void initComponents(View v)
    {

        isEditable = false;
        txtProfileTitle = (TextView)v.findViewById(R.id.txtProfileTitle);
        txtSchoolProfile = (TextView)v.findViewById(R.id.txtSchoolProfile);
        txtNameSchool = (TextView)v.findViewById(R.id.txtNameSchool);
        txtAgeProfile = (TextView)v.findViewById(R.id.txtAgeProfile);
        txtAgeValueProfile = (TextView)v.findViewById(R.id.txtAgeValueProfile);
        txtGenderProfile = (TextView)v.findViewById(R.id.txtGenderProfile);
        txtGenderValueProfile = (TextView)v.findViewById(R.id.txtGenderValueProfile);
        txtFormProfile = (TextView)v.findViewById(R.id.txtFormProfile);
        txtFormValueProfile = (TextView)v.findViewById(R.id.txtFormValueProfile);
        txtEmailProfile = (TextView)v.findViewById(R.id.txtEmailProfile);
        txtEmailValueProfile = (TextView)v.findViewById(R.id.txtEmailValueProfile);
        txtPhoneNumberProfile = (TextView)v.findViewById(R.id.txtPhoneNumberProfile);
        txtPhoneNumberValueProfile = (TextView)v.findViewById(R.id.txtPhoneNumberValueProfile);
        edtEmailValueProfile = (EditText)v.findViewById(R.id.edtEmailValueProfile);
        edtPhoneNumberValueProfile = (EditText)v.findViewById(R.id.edtPhoneNumberValueProfile);
        btnLogout = (Button)v.findViewById(R.id.btnLogout);
        btnSaveInfo = (Button)v.findViewById(R.id.btnSaveInfo);
        imgProfile = (ImageView)v.findViewById(R.id.imgProfile);


        putTypography();

        imgProfile.setOnClickListener(this);
        btnLogout.setOnClickListener(this);
        btnSaveInfo.setOnClickListener(this);
        txtEmailValueProfile.setOnClickListener(this);
        txtPhoneNumberValueProfile.setOnClickListener(this);


        btnSaveInfo.requestFocus();
    }

    private void putTypography()
    {
        Typeface myTypeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand_Bold.otf");
        Typeface myTypeface2 = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand_Book.otf");


        txtProfileTitle.setTypeface(myTypeface);
        txtSchoolProfile.setTypeface(myTypeface);
        txtAgeProfile.setTypeface(myTypeface);
        txtGenderProfile.setTypeface(myTypeface);
        txtFormProfile.setTypeface(myTypeface);
        txtEmailProfile.setTypeface(myTypeface);
        txtPhoneNumberProfile.setTypeface(myTypeface);
        edtPhoneNumberValueProfile.setTypeface(myTypeface);
        edtEmailValueProfile.setTypeface(myTypeface);
        btnLogout.setTypeface(myTypeface);
        btnSaveInfo.setTypeface(myTypeface);

        txtNameSchool.setTypeface(myTypeface2);
        txtAgeValueProfile.setTypeface(myTypeface2);
        txtGenderValueProfile.setTypeface(myTypeface2);
        txtFormValueProfile.setTypeface(myTypeface2);
        txtEmailValueProfile.setTypeface(myTypeface2);
        txtPhoneNumberValueProfile.setTypeface(myTypeface2);
    }


    @Override
    public void onClick(View v)
    {
        Intent i;
        switch (v.getId())
        {
            case R.id.btnLogout:
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                getActivity().finish();
                break;
            case R.id.imgProfile:
                createDialog();
                break;
            case R.id.btnPhotoLibrary:
                i = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(i, LOAD_FROM_GALLERY);
                dialog.dismiss();
                break;
            case R.id.btnTakeFromCamera:
                 i = new Intent("android.media.action.IMAGE_CAPTURE");
                String root = Environment.getExternalStorageDirectory().toString();
                File file = new File(root + "/.steriome/profile_picture"+File.separator+"image_parser.jpg");
                i.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));

                if (i.resolveActivity(getActivity().getPackageManager()) != null)
                {
                    startActivityForResult(i, LOAD_FROM_CAMERA);
                }
                dialog.dismiss();
                break;
            case R.id.txtEmailValueProfile:
            case R.id.txtPhoneNumberValueProfile:
                makeEditable();
                break;
            case R.id.btnSaveInfo:
                makeStatic();
                break;
        }
    }

    private void makeStatic()
    {

        btnSaveInfo.setVisibility(View.GONE);
        txtPhoneNumberValueProfile.setVisibility(View.VISIBLE);
        txtEmailValueProfile.setVisibility(View.VISIBLE);
        edtEmailValueProfile.setVisibility(View.GONE);
        edtPhoneNumberValueProfile.setVisibility(View.GONE);

        txtPhoneNumberValueProfile.setText(edtPhoneNumberValueProfile.getText().toString());
        txtEmailValueProfile.setText(edtEmailValueProfile.getText().toString());

        isEditable = false;
    }

    private void makeEditable()
    {
        btnSaveInfo.setVisibility(View.VISIBLE);
        edtEmailValueProfile.setVisibility(View.VISIBLE);
        edtPhoneNumberValueProfile.setVisibility(View.VISIBLE);
        txtPhoneNumberValueProfile.setVisibility(View.GONE);
        txtEmailValueProfile.setVisibility(View.GONE);

        edtEmailValueProfile.setText(txtEmailValueProfile.getText());
        edtPhoneNumberValueProfile.setText(txtPhoneNumberValueProfile.getText());

        isEditable = true;
    }

    private void createDialog()
    {
        Typeface myTypeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand_Bold.otf");
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_pick_picture);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationZoom;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        ((TextView)dialog.findViewById(R.id.txtTitleDialogPickPicture)).setTypeface(myTypeface);
        ((Button)dialog.findViewById(R.id.btnPhotoLibrary)).setTypeface(myTypeface);
        ((Button)dialog.findViewById(R.id.btnTakeFromCamera)).setTypeface(myTypeface);
        dialog.findViewById(R.id.btnPhotoLibrary).setOnClickListener(this);
        dialog.findViewById(R.id.btnTakeFromCamera).setOnClickListener(this);
        dialog.show();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        ImagesUtil imagesUtil = new ImagesUtil();
        UtilFiles utilFiles = new UtilFiles();
        if (resultCode == getActivity().RESULT_OK)
        {
            if(requestCode == LOAD_FROM_CAMERA)
            {
                imgBackground = imagesUtil.getImageFromCamera();
            }
            if(requestCode == LOAD_FROM_GALLERY)
            {
                imgBackground = imagesUtil.getImageFromGallery(data, getActivity().getContentResolver());
            }
            imgBackground = imagesUtil.getRoundedRectBitmap(imgBackground,requestCode,imgProfile,getActivity());
            imgProfile.setImageDrawable(new BitmapDrawable(getResources(), imgBackground));
            utilFiles.saveImage(imgBackground);

        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putBoolean("isEditable",isEditable);
        if(isEditable)
        {
            outState.putString("mail",edtEmailValueProfile.getText().toString());
            outState.putString("phone",edtPhoneNumberValueProfile.getText().toString());
        }
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState)
    {
        super.onViewStateRestored(savedInstanceState);
        if(savedInstanceState!=null&&savedInstanceState.getBoolean("isEditable"))
        {
            makeEditable();
            edtEmailValueProfile.setText(savedInstanceState.getString("mail"));
            edtPhoneNumberValueProfile.setText(savedInstanceState.getString("phone"));
        }
    }
    public void hideSoftKeyboard (Activity activity, View view)
    {
        InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
    }
}
