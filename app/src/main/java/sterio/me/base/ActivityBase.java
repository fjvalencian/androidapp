package sterio.me.base;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.File;

import sterio.me.customview.CircleView;
import sterio.me.steriomeapp.R;
import sterio.me.steriomeapp.insights.activities.InsightsActivity;
import sterio.me.steriomeapp.learning.activities.LearningActivity;
import sterio.me.steriomeapp.notification.activities.NotificationsActivity;
import sterio.me.util.UtilFiles;

/**
 * Created by jacevedo on 19-01-15.
 */
public class ActivityBase extends FragmentActivity implements View.OnClickListener
{
    private CircleView imgButtonLearning;
    private CircleView imgButtonInsinghts;
    private CircleView imgButtonProfile;
    private CircleView imgButtonNotifications;
    private RelativeLayout layoutLearning;
    private RelativeLayout layoutInsinghts;
    private RelativeLayout layoutProfile;
    private RelativeLayout layoutNotifications;
    protected TextView txtLearningTitle;
    protected TextView txtInsightTitle;
    protected TextView txtProfileTitle;
    protected TextView txtNotificationsTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragments);
        initComponents();

    }
    private void initComponents()
    {
        imgButtonLearning = (CircleView)findViewById(R.id.imgButtonLearning);
        imgButtonInsinghts = (CircleView)findViewById(R.id.imgButtonInsinghts);
        imgButtonProfile = (CircleView)findViewById(R.id.imgButtonProfile);
        imgButtonNotifications = (CircleView)findViewById(R.id.imgButtonNotifications);
        layoutLearning = (RelativeLayout)findViewById(R.id.layoutLearningTitle);
        layoutInsinghts = (RelativeLayout)findViewById(R.id.layoutInsinghts);
        layoutProfile = (RelativeLayout)findViewById(R.id.layoutProfile);
        layoutNotifications = (RelativeLayout)findViewById(R.id.layoutNotifications);
        txtLearningTitle = (TextView)findViewById(R.id.txtLearningTitle);
        txtInsightTitle = (TextView)findViewById(R.id.txtInsightTitle);
        txtProfileTitle = (TextView)findViewById(R.id.txtProfileTitle);
        txtNotificationsTitle = (TextView)findViewById(R.id.txtNotificationsTitle);

        imgButtonLearning.setColorFirstCircle(Color.rgb(174,207,116));
        imgButtonInsinghts.setColorFirstCircle(Color.rgb(55,171,152));
        imgButtonProfile.setColorFirstCircle(Color.rgb(232,205,92));
        imgButtonNotifications.setColorFirstCircle(Color.rgb(204,236,100));

        imgButtonLearning.setTextSecondCircle(3);
        imgButtonInsinghts.setTextSecondCircle(0);
        imgButtonProfile.setTextSecondCircle(0);
        imgButtonNotifications.setTextSecondCircle(5);

        layoutLearning.setOnClickListener(this);
        layoutInsinghts.setOnClickListener(this);
        layoutProfile.setOnClickListener(this);
        layoutNotifications.setOnClickListener(this);


        putTypography();
    }

    private void putTypography()
    {
        Typeface myTypeface = Typeface.createFromAsset(getAssets(), "fonts/Quicksand_Bold.otf");
        txtLearningTitle.setTypeface(myTypeface);
        txtInsightTitle.setTypeface(myTypeface);
        txtProfileTitle.setTypeface(myTypeface);
        txtNotificationsTitle.setTypeface(myTypeface);
    }
    protected void openLearning()
    {
        Intent i = new Intent(this, LearningActivity.class);
        startActivity(i);
        finish();
    }
    protected void openInsights()
    {
        Intent i = new Intent(this, InsightsActivity.class);
        startActivity(i);
        finish();
    }
    private void openNotification()
    {
        Intent i = new Intent(this, NotificationsActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public void onClick(View v)
    {
        Intent i;
        switch (v.getId())
        {
            case R.id.layoutLearningTitle:
               openLearning();
                break;
            case R.id.layoutInsinghts:
                openInsights();
                break;
            case R.id.layoutProfile:
                finish();
                break;
            case R.id.layoutNotifications:
                openNotification();
                break;
        }
    }
    public void searchImageProfile(ImageView image)
    {
        String root = Environment.getExternalStorageDirectory().toString();
        String fileRoute = root + "/.steriome/profile_picture/profile_picture.png";
        File myDir = new File(fileRoute);
        UtilFiles utilFiles = new UtilFiles();
        if(myDir.exists())
        {
            image.setImageBitmap(utilFiles.getImageBitmap(fileRoute));
        }
    }
    public void putFonts(TextView... textViews)
    {
        Typeface myTypeface = Typeface.createFromAsset(getAssets(), "fonts/Quicksand_Bold.otf");
        for(TextView textView : textViews)
        {
            textView.setTypeface(myTypeface);
        }
    }
    public void putFonts(EditText... editTexts)
    {
        Typeface myTypeface = Typeface.createFromAsset(getAssets(), "fonts/Quicksand_Bold.otf");
        for(EditText editText : editTexts)
        {
            editText.setTypeface(myTypeface);
        }
    }
    public void putFonts(Button... buttons)
    {
        Typeface myTypeface = Typeface.createFromAsset(getAssets(), "fonts/Quicksand_Bold.otf");
        for(Button button : buttons)
        {
            button.setTypeface(myTypeface);
        }
    }



}
