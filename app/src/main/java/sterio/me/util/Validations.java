package sterio.me.util;

import android.content.Context;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.regex.Pattern;

import sterio.me.adapters.CustomSpinnerAdapter;
import sterio.me.steriomeapp.R;

public class Validations
{
    private Context context;
    public Validations(Context context)
    {
        this.context = context;
    }
	public boolean validateOnlyNumbers(String text)
	{
		if(!Pattern.matches("^[0-9]*", text))
		{
		    return false;
		}
		return true;
	}
	public boolean validateTextAndNumber(String text)
	{
		if(!Pattern.matches("^[a-zA-ZñÑ0-9]*", text) || text.length()==0)
		{
		    return false;
		}
		return true;
	}
	public boolean validateNames(String text)
	{
		if(!Pattern.matches("(\\p{L}+)(([ ])(\\p{L}+))?", text)||text.length()==0)
		{
		    return false;
		}
		return true;
	}
	public boolean validateOnlyText(String text)
	{
		if(!Pattern.matches("^[a-zA-ZñÑ]*$", text))
		{
		    return false;
		}
		return true;
	}
	
	public boolean validateMail(String text)
	{
		if(!Pattern.matches("^[a-zA-Z0-9._%+-]+@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,4}$", text))
		{
		    return false;
		}
		return true;
	}
	public boolean validateFormatRut(String text)
	{
		if(!Pattern.matches("\\b\\d{7,8}\\-[K|k|0-9]", text))
		{
		    return false;
		}
		return true&& validateRut(text);
	}
	private boolean validateRut(String text)
	{
		String[]arrayRut = text.split("-");
		int rut = Integer.parseInt(arrayRut[0]);
		char dv = arrayRut[1].charAt(0);
		int m = 0, s = 1;
        for (; rut != 0; rut /= 10)
        {
            s = (s + rut % 10 * (9 - m++ % 6)) % 11;
        }
       if(dv == (char) (s != 0 ? s + 47 : 75))
       {
    	   return true;
       }
       return false;
	}
	public boolean validateDate(String text)
	{
		if(!Pattern.matches("\\d{2}\\-\\d{2}\\-\\d{4}", text))
		{
		    return false;
		}
		return true;
	}
	public boolean validatePhone(String text)
	{
		if(!Pattern.matches("0{0,2}([\\+]?[\\d]{1,3} ?)?([\\(]([\\d]{2,3})[)] ?)?[0-9][0-9 \\-]{6,}( ?([xX]|([eE]xt[\\.]?)) ?([\\d]{1,5}))?", text))
		{
		    return false;
		}
		return true;
	}
	public boolean ValidateMail(String text)
	{
		if(!Pattern.matches("0{0,2}([\\+]?[\\d]{1,3} ?)?([\\(]([\\d]{2,3})[)] ?)?[0-9][0-9 \\-]{6,}( ?([xX]|([eE]xt[\\.]?)) ?([\\d]{1,5}))?", text))
		{
		    return false;
		}
		return true;
	}

    public boolean validatePassword(String s, String s1)
    {
        if(s.length()>=6 && s.compareTo(s1) == 0)
        {
            return true;
        }
        return false;
    }

    public boolean validateLearnerCode(String s)
    {
        if(s.length()!=0)
        {
            return true;
        }
        return false;

    }
    public void badAnswer(TextView txtBadAnswer, EditText... edtResponse)
    {
        txtBadAnswer.setVisibility(View.VISIBLE);
        for(EditText editText : edtResponse)
        {
            editText.setBackgroundResource(R.drawable.rect_edit_text_bad_validation);
            editText.setHintTextColor(context.getResources().getColor(R.color.alizarin_crimson));
        }
    }
    public void goodAnswer(TextView txtBadAnswer, EditText... edtResponse)
    {
        txtBadAnswer.setVisibility(View.GONE);
        for(EditText editText : edtResponse)
        {
            editText.setBackgroundResource(R.drawable.rect_edit_text);
            editText.setHintTextColor(context.getResources().getColor(R.color.elephant));
        }
    }
    public void badAnswer(TextView txtBadAnswer, Spinner spResponse)
    {
        txtBadAnswer.setVisibility(View.VISIBLE);
        spResponse.setBackgroundResource(R.drawable.rect_spinner_bad_answer);
        ((CustomSpinnerAdapter)spResponse.getAdapter()).setBadAnswer(true);
        ((CustomSpinnerAdapter) spResponse.getAdapter()).notifyDataSetChanged();

    }
    public void goodAnswer(TextView txtBadAnswer, Spinner spResponse)
    {
        txtBadAnswer.setVisibility(View.GONE);
        spResponse.setBackgroundResource(R.drawable.rect_spinner);
        ((CustomSpinnerAdapter)spResponse.getAdapter()).setBadAnswer(false);
    }

}
