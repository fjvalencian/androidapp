package sterio.me.util;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by jacevedo on 29-01-15.
 */
public class PutFonts
{
    private Context context;
    public PutFonts(Context context)
    {
        this.context = context;
    }
    public void putFonts(TextView... textViews)
    {
        Typeface myTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/Quicksand_Bold.otf");
        for(TextView textView : textViews)
        {
            textView.setTypeface(myTypeface);
        }
    }
    public void putFonts(EditText... editTexts)
    {
        Typeface myTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/Quicksand_Bold.otf");
        for(EditText editText : editTexts)
        {
            editText.setTypeface(myTypeface);
        }
    }
    public void putFonts(Button... buttons)
    {
        Typeface myTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/Quicksand_Bold.otf");
        for(Button button : buttons)
        {
            button.setTypeface(myTypeface);
        }
    }
}
