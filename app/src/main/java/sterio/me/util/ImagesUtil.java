package sterio.me.util;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;

import java.io.File;
import java.io.IOException;

import sterio.me.steriomeapp.R;

/**
 * Created by jacevedo on 28-01-15.
 */
public class ImagesUtil
{
    private static final int LOAD_FROM_GALLERY = 0;
    private static final int LOAD_FROM_CAMERA = 1;

    public Bitmap getImageFromCamera()
    {
        String root = Environment.getExternalStorageDirectory().toString();
        File file = new File(root + "/.steriome/profile_picture"+File.separator+"image_parser.jpg");
        int rotate = getOrientation(file);

        Bitmap bitmap = decodeSampledBitmapFromFile(file.getAbsolutePath(), 1000, 700);
        Matrix matrix = new Matrix();
        matrix.postRotate(rotate);

        bitmap = Bitmap.createBitmap(bitmap , 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);

        file.delete();
        return bitmap;
    }
    public Bitmap getImageFromGallery(Intent data,ContentResolver content)
    {
        Bitmap bm = null;
        try
        {
            Uri selectedImageUri = data.getData();
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            bm = BitmapFactory.decodeFile(getImage(selectedImageUri,content));

        }
        catch (Exception e)
        {
            Log.e("Open Picture", e.toString());
        }
        return  bm;
    }
    private String getImage(Uri uri,ContentResolver content)
    {
        String[] filePathColumn = { MediaStore.Images.Media.DATA };

        Cursor cursor = content.query(uri,
                filePathColumn, null, null, null);
        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String picturePath = cursor.getString(columnIndex);
        cursor.close();
        return picturePath;
    }
    public static Bitmap decodeSampledBitmapFromFile(String path, int reqWidth, int reqHeight)
    { // BEST QUALITY MATCH

        //First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize, Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        int inSampleSize = 1;

        if (height > reqHeight)
        {
            inSampleSize = Math.round((float)height / (float)reqHeight);
        }
        int expectedWidth = width / inSampleSize;

        if (expectedWidth > reqWidth)
        {
            //if(Math.round((float)width / (float)reqWidth) > inSampleSize) // If bigger SampSize..
            inSampleSize = Math.round((float)width / (float)reqWidth);
        }

        options.inSampleSize = inSampleSize;

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(path, options);
    }
    public Bitmap getRoundedRectBitmap(Bitmap bitmap,int from, View container,Context context)
    {
        int w = container.getWidth();
        int h = container.getHeight();



        int imageWidth = bitmap.getWidth();
        int imageHeight = bitmap.getHeight();

        float scaleFactor = Math.max((float) w / imageWidth,
                (float) h / imageHeight);


        int radius = Math.min(h / 2, w / 2);
        Bitmap output = Bitmap.createBitmap(w + 8, h + 8, Bitmap.Config.ARGB_8888);

        Paint p = new Paint();
        p.setAntiAlias(true);

        Canvas c = new Canvas(output);
        c.drawARGB(0, 0, 0, 0);
        p.setStyle(Paint.Style.FILL);

        c.drawCircle((w / 2) + 4, (h / 2) + 4, radius, p);

        p.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));

        Bitmap scaled = Bitmap.createScaledBitmap(  bitmap,
                (int)(scaleFactor * imageWidth),
                (int)(scaleFactor * imageHeight),
                true );


       c.drawBitmap(scaled, 0,0 , p);



        p.setXfermode(null);
        p.setStyle(Paint.Style.STROKE);
        p.setColor(context.getResources().getColor(R.color.elephant));
        p.setStrokeWidth(3);
        c.drawCircle((w / 2) + 4, (h / 2) + 4, radius, p);

        return output;
    }
    private int getOrientation(File imageFile)
    {
        int rotate = 0;
        ExifInterface exif = null;
        try
        {
            exif = new ExifInterface(
                    imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            switch (orientation)
            {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return rotate;
    }
}
