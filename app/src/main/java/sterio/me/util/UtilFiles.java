package sterio.me.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by jacevedo on 28-01-15.
 */
public class UtilFiles
{
    public void saveImage(Bitmap finalBitmap)
    {
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/.steriome/profile_picture");
        if(!myDir.exists())
        {
            myDir.mkdirs();
        }
        String fName = "profile_picture.png";
        File file = new File (myDir, fName);
        if (file.exists ())
        {
            file.delete ();
        }
        try
        {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.flush();
            out.close();

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    public Bitmap getImageBitmap(String fileRoute)
    {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap bitmap = BitmapFactory.decodeFile(fileRoute, options);
        return bitmap;
    }
}
