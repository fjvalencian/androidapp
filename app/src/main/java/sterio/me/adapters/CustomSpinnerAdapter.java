package sterio.me.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import sterio.me.steriomeapp.R;

/**
 * Created by jacevedo on 19-01-15.
 */
public class CustomSpinnerAdapter extends ArrayAdapter<String>
{

    private Context context;
    private String[] text;
    private boolean isBadAnswer;

    public CustomSpinnerAdapter(Context context, int textViewResourceId,String[] text)
    {
        super(context, textViewResourceId,text);
        this.context = context;
        this.text = text;
        isBadAnswer = false;
    }
    @Override
    public View getDropDownView(int position, View convertView,ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row=mInflater.inflate(R.layout.row_spiner, parent, false);

        Typeface myTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/Quicksand_Bold.otf");
        TextView txtSpinnerText =(TextView)row.findViewById(R.id.txtSpinnerText);
        txtSpinnerText.setText(text[position]);
        txtSpinnerText.setTypeface(myTypeface);
        if(isBadAnswer && position == 0)
        {
            txtSpinnerText.setTextColor(context.getResources().getColor(R.color.monza));
        }
        else
        {
            txtSpinnerText.setTextColor(context.getResources().getColor(R.color.elephant));
        }

        return row;
    }
    public void setBadAnswer(boolean isBadAnswer)
    {
        this.isBadAnswer = isBadAnswer;
    }
}

