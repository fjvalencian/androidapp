package sterio.me.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import sterio.me.object.adapter.InsightsScoreObject;
import sterio.me.steriomeapp.R;

/**
 * Created by jacevedo on 22-01-15.
 */
public class InsightsAdapter extends BaseAdapter
{
    private ArrayList<InsightsScoreObject> listScore;
    private Context context;

    public InsightsAdapter(ArrayList<InsightsScoreObject> listScore, Context context)
    {
        this.listScore = listScore;
        this.context = context;
    }
    @Override
    public int getCount()
    {
        return listScore.size();
    }

    @Override
    public Object getItem(int position)
    {
        return listScore.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return listScore.get(position).getModuleName().hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = mInflater.inflate(R.layout.score_list_item, null);

        TextView txtModuleName = ((TextView) v.findViewById(R.id.txtModuleName));
        TextView txtScore = ((TextView)v.findViewById(R.id.txtScore));
        txtModuleName.setText(listScore.get(position).getModuleName());
        txtScore.setText(listScore.get(position).getScore());
        Button btnViewScore = (Button)v.findViewById(R.id.btnViewScore);
        if(listScore.get(position).getType() == 1)
        {
            putTypography(txtModuleName, txtScore, btnViewScore);
        }
        else
        {
            btnViewScore.setVisibility(View.GONE);
            putTypographyTitle(txtModuleName, txtScore);
        }

        return v;
    }

    private void putTypographyTitle(TextView txtModuleName, TextView txtScore)
    {

        Typeface myTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/Quicksand_Bold.otf");
        txtModuleName.setTypeface(myTypeface);
        txtScore.setTypeface(myTypeface);
    }

    private void putTypography(TextView txtModuleName, TextView txtScore, Button btnViewScore)
    {
        Typeface myTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/Quicksand_Book.otf");
        txtScore.setTypeface(myTypeface);
        txtModuleName.setTypeface(myTypeface);
        btnViewScore.setTypeface(myTypeface);

    }
}
