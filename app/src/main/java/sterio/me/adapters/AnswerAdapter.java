package sterio.me.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import sterio.me.object.adapter.BaseQuestionObject;
import sterio.me.object.adapter.MultipleOptionQuestionObject;
import sterio.me.object.adapter.TextQuestionObject;
import sterio.me.object.adapter.VoiceQuestionObject;
import sterio.me.steriomeapp.R;

/**
 * Created by jacevedo on 27-01-15.
 */
public class AnswerAdapter extends BaseAdapter
{
    private List<BaseQuestionObject> listQuestion;
    private Context context;


    public AnswerAdapter(List<BaseQuestionObject>listQuestion , Context context)
    {
        this.listQuestion = listQuestion;
        this.context = context;
    }

    @Override
    public int getCount()
    {
        return listQuestion.size();
    }

    @Override
    public Object getItem(int position)
    {
        return listQuestion.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return listQuestion.get(position).getQuestion().hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if(listQuestion.get(position).whoAmi().compareTo("TextQuestion")==0)
        {
            return getViewResponse((TextQuestionObject)listQuestion.get(position));
        }
        else if(listQuestion.get(position).whoAmi().compareTo("VoiceOption")==0)
        {
            return getViewResponse((VoiceQuestionObject)listQuestion.get(position));
        }
        else if(listQuestion.get(position).whoAmi().compareTo("MultipleOption")==0)
        {
            return getViewResponse((MultipleOptionQuestionObject)listQuestion.get(position));
        }
        return null;

    }

    private View getViewResponse(TextQuestionObject textQuestionObject)
    {
        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = mInflater.inflate(R.layout.answer_text_question, null);
        TextView txtQuestionText = (TextView) v.findViewById(R.id.txtQuestionText);
        EditText edtResponseTextQuestion = (EditText) v.findViewById(R.id.edtResponseTextQuestion);

        txtQuestionText.setText(textQuestionObject.getQuestion());
        edtResponseTextQuestion.setText(textQuestionObject.getResponse());

        putTypography(txtQuestionText);
        putTypography(edtResponseTextQuestion);

        if(textQuestionObject.isGoodAnswer())
        {
            putGoodAnswer(((ImageView) v.findViewById(R.id.imgStateResponse)));
        }

        return v;
    }

    private View getViewResponse(VoiceQuestionObject voiceQuestionObject)
    {
        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = mInflater.inflate(R.layout.answer_voice_question, null);
        TextView txtQuestion = (TextView)v.findViewById(R.id.txtQuestion);
        TextView txtListen = (TextView)v.findViewById(R.id.txtListen);

        txtQuestion.setText(voiceQuestionObject.getQuestion());

        putTypography(txtQuestion, txtListen);
        if(voiceQuestionObject.isGoodAnswer())
        {
            putGoodAnswer(((ImageView) v.findViewById(R.id.imgResponse)));
        }
        return v;
    }

    private View getViewResponse(MultipleOptionQuestionObject multipleOptionQuestionObject)
    {
        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = mInflater.inflate(R.layout.answer_multiple_choice_view, null);
        TextView txtQuestion = (TextView)v.findViewById(R.id.txtQuestion);
        txtQuestion.setText(multipleOptionQuestionObject.getQuestion());
        putCorrectCircle(multipleOptionQuestionObject.getCorrectQuestion(), v);
        putIncorrectCircle(multipleOptionQuestionObject.getCorrectQuestion(), multipleOptionQuestionObject.getSelectedQuestion(), v);
        putTypography(txtQuestion);
        return v;
    }

    private void putIncorrectCircle(int correctQuestion, int selectedQuestion, View v)
    {
        if(correctQuestion!=selectedQuestion)
        {
            switch (selectedQuestion)
            {
                case 1:
                    putBadAnswer(((ImageView) v.findViewById(R.id.imgResponseOption1)));
                    break;
                case 2:
                    putBadAnswer(((ImageView) v.findViewById(R.id.imgResponseOption2)));
                    break;
                case 3:
                    putBadAnswer(((ImageView) v.findViewById(R.id.imgResponseOption3)));
                    break;
                case 4:
                    putBadAnswer(((ImageView) v.findViewById(R.id.imgResponseOption4)));
                    break;
            }
        }
    }

    private void putCorrectCircle(int correctQuestion, View v)
    {
        switch (correctQuestion)
        {
            case 1:
                putGoodAnswer(((ImageView) v.findViewById(R.id.imgResponseOption1)));
                break;
            case 2:
                putGoodAnswer(((ImageView) v.findViewById(R.id.imgResponseOption2)));
                break;
            case 3:
                putGoodAnswer(((ImageView) v.findViewById(R.id.imgResponseOption3)));
                break;
            case 4:
                putGoodAnswer(((ImageView)v.findViewById(R.id.imgResponseOption4)));
                break;
        }
    }
    private void putBadAnswer(ImageView image)
    {
        image.setBackgroundResource(R.drawable.circle_bad_aswer);
        image.setImageResource(R.drawable.cross_without_margin);
    }
    private void putGoodAnswer(ImageView image)
    {
        image.setBackgroundResource(R.drawable.circle_good_aswer);
        image.setImageResource(R.drawable.tick_without_margin);

    }
    private void putTypography(TextView ... views)
    {
        Typeface myTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/Quicksand_Bold.otf");
        for (TextView v : views)
        {
            v.setTypeface(myTypeface);
        }
    }
    private void putTypography(EditText ... views)
    {
        Typeface myTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/Quicksand_Bold.otf");
        for (EditText v : views)
        {
            v.setTypeface(myTypeface);
        }
    }
}
